package com.jappit.midmaps.test;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;
import javax.microedition.midlet.MIDlet;

public class GoogleMapsTestList extends List implements CommandListener
{
	MIDlet midlet;
	
	Command exit;
	
	public GoogleMapsTestList(MIDlet m)
	{
		super("Test list", List.IMPLICIT);
		
		addCommand(exit = new Command("Exit", Command.EXIT, 1));
		
		this.midlet = m;
		
		setCommandListener(this);
		
		append("Simple Example", null);
		append("Using Markers", null);
		append("Using Paths", null);
		append("Using Geocoding", null);
		append("Moving maps", null);
		append("Zooming maps", null);
		append("Using map in a Form", null);
	}
	public void commandAction(Command c, Displayable d)
	{
		if(c == exit)
		{
			midlet.notifyDestroyed();
		}
		else if(c == List.SELECT_COMMAND)
		{
			Displayable next = null;
			
			System.out.println("D: " + getSelectedIndex());
			
			switch(getSelectedIndex())
			{
			case 0:
				next = new GoogleMapsSimpleCanvas(midlet, this);
				break;
			case 1:
				next = new GoogleMapsMarkerCanvas(midlet, this);
				break;
			case 2:
				next = new GoogleMapsPathCanvas(midlet, this);
				break;
			case 3:
				next = new GoogleMapsGeocodeCanvas(midlet, this);
				break;
			case 4:
				next = new GoogleMapsMoveCanvas(midlet, this);
				break;
			case 5:
				//next = new GoogleMapsZoomCanvas(midlet, this);
				break;
			case 6:
				next = new GoogleMapsTestForm(midlet, this);
				break;
			}
			if(d != null)
			{
				Display.getDisplay(midlet).setCurrent(next);
			}
		}
	}
}
