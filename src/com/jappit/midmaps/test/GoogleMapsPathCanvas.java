package com.jappit.midmaps.test;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.midlet.MIDlet;


import com.jappit.midmaps.googlemaps.GoogleMaps;
import com.jappit.midmaps.googlemaps.GoogleMapsCoordinates;
import com.jappit.midmaps.googlemaps.GoogleMapsGeocoderHandler;
import com.jappit.midmaps.googlemaps.GoogleMapsMarker;
import com.jappit.midmaps.googlemaps.GoogleMapsPath;
import com.jappit.midmaps.googlemaps.GoogleStaticMap;
import com.jappit.midmaps.googlemaps.GoogleStaticMapHandler;

public class GoogleMapsPathCanvas extends GoogleMapsTestCanvas implements GoogleStaticMapHandler
{
	GoogleMaps gMaps = null;
	
	GoogleStaticMap map = null;
	
	public GoogleMapsPathCanvas(MIDlet m, Displayable testListScreen)
	{
		super(m, testListScreen);
		
		gMaps = new GoogleMaps();
		
		map = gMaps.createMap(getWidth(), getHeight(), GoogleStaticMap.FORMAT_PNG);
		
		map.setHandler(this);
		
		map.setCenter(new GoogleMapsCoordinates(41.8954656, 12.4823243));
		
		map.setZoom(15);
		
		GoogleMapsPath path = new GoogleMapsPath();
		path.addPoint(new GoogleMapsCoordinates(41.8954656, 12.4823243));
		path.addPoint(new GoogleMapsCoordinates(41.8934656, 12.4833243));
		path.addPoint(new GoogleMapsCoordinates(41.8944656, 12.4843243));
		path.setColor(GoogleStaticMap.COLOR_RED);
		path.setWeight(10);
		map.addPath(path);
		
		GoogleMapsPath bluePath = new GoogleMapsPath();
		bluePath.addPoint(new GoogleMapsCoordinates(41.8954656, 12.4823243));
		bluePath.addPoint(new GoogleMapsCoordinates(41.8964656, 12.4813243));
		bluePath.addPoint(new GoogleMapsCoordinates(41.8934656, 12.4803243));
		bluePath.setColor(GoogleStaticMap.COLOR_BLUE);
		bluePath.setFillColor(GoogleStaticMap.COLOR_GREEN);
		bluePath.setWeight(5);
		map.addPath(bluePath);
		
		map.update();
	}
	
	protected void paint(Graphics g)
	{
		map.draw(g, 0, 0, Graphics.TOP | Graphics.LEFT);
	}
	
	public void GoogleStaticMapUpdateError(GoogleStaticMap map, int errorCode, String errorMessage)
	{
		showError("map error: " + errorCode + ", " + errorMessage);
	}
	public void GoogleStaticMapUpdated(GoogleStaticMap map)
	{
		repaint();
	}
}