package com.jappit.midmaps.test;

import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.midlet.MIDlet;


import com.jappit.midmaps.googlemaps.GoogleMaps;
import com.jappit.midmaps.googlemaps.GoogleMapsCoordinates;
import com.jappit.midmaps.googlemaps.GoogleMapsGeocoder;
import com.jappit.midmaps.googlemaps.GoogleMapsGeocoderHandler;
import com.jappit.midmaps.googlemaps.GoogleMapsMarker;
import com.jappit.midmaps.googlemaps.GoogleMapsPath;
import com.jappit.midmaps.googlemaps.GoogleStaticMap;
import com.jappit.midmaps.googlemaps.GoogleStaticMapHandler;

public class GoogleMapsGeocodeCanvas extends GoogleMapsTestCanvas implements GoogleStaticMapHandler, GoogleMapsGeocoderHandler
{
	GoogleMaps gMaps = null;
	
	GoogleStaticMap map = null;
	
	static String API_KEY = "AIzaSyCj89QbaK6TT8x9JI548kh0pm2l0jc8AgA";
	
	public GoogleMapsGeocodeCanvas(MIDlet m, Displayable testListScreen)
	{
		super(m, testListScreen);
		
		gMaps = new GoogleMaps(API_KEY);
		
		map = gMaps.createMap(getWidth(), getHeight(), GoogleStaticMap.FORMAT_PNG);
		
		map.setHandler(this);
		
		GoogleMapsGeocoder geocoder = gMaps.createGeocoder();
		
		geocoder.setHandler(this);
		
		geocoder.geocodeAddress("Tunis");
	}
	
	protected void paint(Graphics g)
	{
		map.draw(g, 0, 0, Graphics.TOP | Graphics.LEFT);
	}
	
	public void GoogleMapsGeocodeError(String address, int errorCode, String errorDescription)
	{
		System.out.println("Geocode error: " + errorCode + ", " + errorDescription);
	}
	public void GoogleMapsGeocodeSuccess(String address, GoogleMapsCoordinates coordinates, int accuracy)
	{
		map.setCenter(coordinates);
		
		map.addMarker(new GoogleMapsMarker(coordinates));
		
		map.setZoom(accuracy);
		
		map.update();
	}
	public void GoogleStaticMapUpdateError(GoogleStaticMap map, int errorCode, String errorMessage)
	{
		showError("map error: " + errorCode + ", " + errorMessage);
	}
	public void GoogleStaticMapUpdated(GoogleStaticMap map)
	{
		System.out.println("map loaded");
		
		repaint();
	}
	
}