/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.HANDLER;

import java.util.Vector;
import mooc.ENTITIES.Cours;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Anouar
 */
public class CourssssHandler extends DefaultHandler {

    private Vector coursVector;

    public CourssssHandler() {
        coursVector = new Vector();
    }

    public Cours[] getcours() {
        Cours[] courTab = new Cours[coursVector.size()];
        coursVector.copyInto(courTab);
        return courTab;
    }

    private Cours currentCours;

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("cours")) {
            
            String idCour = attributes.getValue("id");
            String idCategorie= attributes.getValue("categorie_id");
            String idFormateur= attributes.getValue("formateur_id");
            String titre = attributes.getValue("titre");
            String description = attributes.getValue("description");
            String DureeDeCours = attributes.getValue("dureedecours");
            String difficulte = attributes.getValue("difficulte");
            currentCours = new Cours();
            currentCours.setId(Integer.parseInt(idCour));
            currentCours.setCategorie(Integer.parseInt(idCategorie));
            currentCours.setFormateur(Integer.parseInt(idCour));
            currentCours.setTitre(titre);
            currentCours.setDescription(description);
            currentCours.setDureedecours(Integer.parseInt(DureeDeCours));
            currentCours.setDifficulte(difficulte);

            if (titre == null || description == null ||  difficulte == null) {
                throw new IllegalArgumentException("cours requires both ");
            }

        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("cours")) {
            // add completed Person object to collection
            coursVector.addElement(currentCours);

            // we are no longer processing a <person.../> tag
            currentCours = null;
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {

    }

}
