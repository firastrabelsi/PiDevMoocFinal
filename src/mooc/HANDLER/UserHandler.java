package mooc.HANDLER;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Vector;
import mooc.ENTITIES.User;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class UserHandler extends DefaultHandler {

    // this will hold all the data we read

    private Vector peopleVector;

    public UserHandler() {
        peopleVector = new Vector();
    }

    public User[] getPeople() {
        User[] personTab = new User[peopleVector.size()];
        peopleVector.copyInto(personTab);
        return personTab;
    }

    // VARIABLES TO MAINTAIN THE PARSER'S STATE DURING PROCESSING
    private User currentPerson;
    private String currentBalise;
    //private PhoneNumber currentPhoneNumber;

    // XML EVENT PROCESSING METHODS (DEFINED BY DefaultHandler)
    // startElement is the opening part of the tag "<tagname...>"
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("User")) {
            currentPerson = new User();
            currentBalise = "User";

        }
        if (qName.equals("id")) {
            currentBalise = "Id";
        }

        if (qName.equals("username")) {
            currentBalise = "username";
        }
        if (qName.equals("email")) {
            currentBalise = "email";
        }
        if (qName.equals("password")) {
            currentBalise = "password";
        }
        if (qName.equals("etat")) {
            currentBalise = "etat";
        }
        if (qName.equals("nom")) {
            currentBalise = "nom";
        }
        if (qName.equals("prenom")) {
            currentBalise = "prenom";
        }
        if (qName.equals("adresse")) {
            currentBalise = "adresse";
        }
        if (qName.equals("url")) {
            currentBalise = "url";
        }
        if (qName.equals("role")) {
            currentBalise = "role";
        }
        if (qName.equals("nomDeLaSociete")) {
            currentBalise = "nomDeLaSociete";
        }
        if (qName.equals("site")) {
            currentBalise = "site";
        }
        if (qName.equals("certificat")) {
            currentBalise = "certificat";
        }
        if (qName.equals("specialite")) {
            currentBalise = "specialite";
        }
        if (qName.equals("num")) {
            currentBalise = "num";
        }
           if (qName.equals("etat_c")) {
            currentBalise = "etat_c";
        }
    }

    // endElement is the closing part ("</tagname>"), or the opening part if it ends with "/>"
    // so, a tag in the form "<tagname/>" generates both startElement() and endElement()
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("User")) {
            // add completed Person object to collection
            peopleVector.addElement(currentPerson);

            // we are no longer processing a <person.../> tag
            currentPerson = null;
        }
    }

    // "characters" are the text inbetween tags
    public void characters(char ch[], int start, int length) {
        String E = new String(ch, start, length).trim();
        if (currentBalise.equals("Id")) {

            currentPerson.setId(Integer.parseInt(E));

        }
        if (currentBalise.equals("username")) {
            currentPerson.setUsername(E);
        }
        if (currentBalise.equals("password")) {
            currentPerson.setPassword(E);
        }
        if (currentBalise.equals("email")) {
            currentPerson.setEmail(E);
        }
        if (currentBalise.equals("adresse")) {
            currentPerson.setAdresse(E);
        }
        if (currentBalise.equals("url")) {
            currentPerson.setUrl(E);
        }
        if (currentBalise.equals("etat")) {
            currentPerson.setEtat(E);
        }
        if (currentBalise.equals("nom")) {
            currentPerson.setNom(E);
        }
        if (currentBalise.equals("prenom")) {
            currentPerson.setPrenom(E);
        }
        if (currentBalise.equals("role")) {
            currentPerson.setRole(E);
        }
        if (currentBalise.equals("nomDeLaSociete")) {
            currentPerson.setNomsociete(E);
        }
        if (currentBalise.equals("site")) {
            currentPerson.setSite(E);
        }
        if (currentBalise.equals("certificat")) {
            currentPerson.setCertificat(E);
        }
        if (currentBalise.equals("specialite")) {
            currentPerson.setSpecialite(E);
        }
        if (currentBalise.equals("num")) {
            currentPerson.setNum(Integer.parseInt(E));
        }
        if (currentBalise.equals("etat_c")) {
            currentPerson.setEtat_c(Integer.parseInt(E));
        }

    }
}
