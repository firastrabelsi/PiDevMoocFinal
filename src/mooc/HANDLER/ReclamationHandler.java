package mooc.HANDLER;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Vector;
import mooc.ENTITIES.Reclamation;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ReclamationHandler extends DefaultHandler {

    // this will hold all the data we read
    private Vector reclamationVector;

    public ReclamationHandler() {
        reclamationVector = new Vector();
    }

    public Reclamation[] getreclamation() {
        Reclamation[] personTab = new Reclamation[reclamationVector.size()];
        reclamationVector.copyInto(personTab);
        return personTab;
    }

    // VARIABLES TO MAINTAIN THE PARSER'S STATE DURING PROCESSING
    private Reclamation currentreclamation;
    private String currentBalise;
    //private PhoneNumber currentPhoneNumber;

    // XML EVENT PROCESSING METHODS (DEFINED BY DefaultHandler)
    // startElement is the opening part of the tag "<tagname...>"
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("Reclamation")) {
            currentreclamation = new Reclamation();
            currentBalise = "Reclamation";

        }
        if (qName.equals("id")) {
            currentBalise = "id";
        }
        if (qName.equals("email")) {
            currentBalise = "email";
        }
        if (qName.equals("categorie")) {
            currentBalise = "categorie";
        }

        if (qName.equals("text")) {
            currentBalise = "text";
        }
        if (qName.equals("etat")) {
            currentBalise = "etat";
        }
        if (qName.equals("tel")) {
            currentBalise = "tel";
        }
        if (qName.equals("date")) {
            currentBalise = "date";
        }

        if (qName.equals("iduser")) {
            currentBalise = "iduser";
        }

    }

    // endElement is the closing part ("</tagname>"), or the opening part if it ends with "/>"
    // so, a tag in the form "<tagname/>" generates both startElement() and endElement()
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("Reclamation")) {
            // add completed Person object to collection
            reclamationVector.addElement(currentreclamation);

            // we are no longer processing a <person.../> tag
            currentreclamation = null;
        }
    }

    // "characters" are the text inbetween tags
    public void characters(char ch[], int start, int length) {
        String E = new String(ch, start, length).trim();
        if (currentBalise.equals("id")) {

            currentreclamation.setId(Integer.parseInt(E));

        }
        if (currentBalise.equals("categorie")) {
            currentreclamation.setCategorie(E);
        }
        if (currentBalise.equals("text")) {
            currentreclamation.setText(E);
        }
        if (currentBalise.equals("email")) {
            currentreclamation.setEmail(E);
        }
        if (currentBalise.equals("etat")) {
            currentreclamation.setEtat(E);
        }
        if (currentBalise.equals("date")) {
            currentreclamation.setDate(E);
        }
        if (currentBalise.equals("iduser")) {
            currentreclamation.setIduser(Integer.parseInt(E));
        }
        if (currentBalise.equals("tel")) {
            currentreclamation.setTel(Integer.parseInt(E));
        }

    }
}
