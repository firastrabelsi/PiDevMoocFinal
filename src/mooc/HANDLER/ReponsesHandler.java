/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mooc.HANDLER;

import java.util.Vector;
import mooc.ENTITIES.Reponse;
import mooc.ENTITIES.Sujet;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Aymen
 */
public class ReponsesHandler extends DefaultHandler{
    private Vector reponses;
    String idTag = "close";
    String dateTag = "close";
    String texteTag = "close";
    String id_userTag = "close";
    String id_sujetTag = "close";
    
    public ReponsesHandler(){
        
        reponses = new Vector();
    
    }
    
    public Reponse[] getReponse(){
        Reponse[] reponsess = new Reponse[reponses.size()];
        reponses.copyInto(reponsess);
        return reponsess;
    
 }
    
    private Reponse currentReponse;
    
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("reponse")) {
            currentReponse = new Reponse();
        } else if (qName.equals("id")) {
            idTag = "open";
        } else if (qName.equals("date")) {
            dateTag = "open";
        } else if (qName.equals("texte")) {
            texteTag = "open";
        } else if (qName.equals("id_user")) {
            id_userTag = "open";
        } else if (qName.equals("id_sujet")) {
            id_sujetTag = "open";
        }
    }
    
    
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("reponse")) {
            // we are no longer processing a <reg.../> tag
            reponses.addElement(currentReponse);
            currentReponse = null;
        } else if (qName.equals("id")) {
            idTag = "close";
        } else if (qName.equals("date")) {
            dateTag = "close";
        } else if (qName.equals("texte")) {
            texteTag = "close";
        } else if (qName.equals("id_user")) {
            id_userTag = "close";
        } else if (qName.equals("id_sujet")) {
            id_sujetTag = "close";
        }

    }
    
    public void characters(char[] ch, int start, int length) throws SAXException {
        // we're only interested in this inside a <phone.../> tag
        if (currentReponse != null) {
            // don't forget to trim excess spaces from the ends of the string
            if (idTag.equals("open")) {
                String id = new String(ch, start, length).trim();
                currentReponse.setId(Integer.parseInt(id));
            } else if (dateTag.equals("open")) {
                String date = new String(ch, start, length).trim();
                currentReponse.setDate(date);
            } else if (texteTag.equals("open")) {
                String texte = new String(ch, start, length).trim();
                currentReponse.setTexte(texte);
            } else if (id_userTag.equals("open")) {
                String id_user = new String(ch, start, length).trim();
                currentReponse.setId_user(Integer.parseInt(id_user));
            } else if (id_sujetTag.equals("open")) {
                String id_sujet = new String(ch, start, length).trim();
                currentReponse.setId_sujet(Integer.parseInt(id_sujet));
               
            }
        }
    
    
    
} 
    
}    
