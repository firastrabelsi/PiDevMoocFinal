/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.HANDLER;

import  mooc.ENTITIES.Chapitre;
import  mooc.ENTITIES.Cours;
import java.io.DataInputStream;
import java.util.Vector;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Dell
 */
public class ChapitreHandler extends DefaultHandler {

    private Vector chapitre;
    String idChapTag = "close";
    String NomTag = "close";
    String DescriptionTag = "close";
    String id_coursTag = "close";

    public ChapitreHandler() {
        chapitre = new Vector();
    }

    public Chapitre[] getChapitre() {
        Chapitre[] chapitree = new Chapitre[chapitre.size()];
        chapitre.copyInto(chapitree);
        return chapitree;
    }
    // VARIABLES TO MAINTAIN THE PARSER'S STATE DURING PROCESSING
    private Chapitre currentChapitre;

    ////////////////////////////////
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("mydata")) {

            if (currentChapitre != null) {
                throw new IllegalStateException("already processing a cours");
            }
            currentChapitre = new Chapitre();
        } else if (qName.equals("idChap")) {
            idChapTag = "open";

        } else if (qName.equals("Nom")) {
            NomTag = "open";
        } else if (qName.equals("Description")) {
            DescriptionTag = "open";
        } else if (qName.equals("id_cours")) {
            id_coursTag = "open";
        }
    }

////////////////////////////////////////
    public void endElement(String uri, String localName, String qName) throws SAXException {

        if (qName.equals("mydata")) {
            // we are no longer processing a <reg.../> tag
            chapitre.addElement(currentChapitre);
            currentChapitre = null;
        } else if (qName.equals("idChap")) {
            idChapTag = "close";
        } else if (qName.equals("Nom")) {
            NomTag = "close";
        } else if (qName.equals("Description")) {
            DescriptionTag = "close";
        } else if (qName.equals("id_cours")) {
            id_coursTag = "close";
        }
    }

    ///////////////////////////////////////////
    public void characters(char[] ch, int start, int length) throws SAXException {
        // we're only interested in this inside a <phone.../> tag
        if (currentChapitre != null) {
            if (idChapTag.equals("open")) {
                int idChap = Integer.parseInt(new String(ch, start, length).trim());
                currentChapitre.setId(idChap);
            
           // don't forget to trim excess spaces from the ends of the string
            } if (NomTag.equals("open")) {
                String Nom = new String(ch, start, length).trim();
                currentChapitre.setNom(Nom);
            } else if (DescriptionTag.equals("open")) {
                String Description = new String(ch, start, length).trim();
                currentChapitre.setDescription(Description);
            } else if (id_coursTag.equals("open")) {
                int id_cours = Integer.parseInt(new String(ch, start, length).trim());
                Cours[] offre_table = null;
                String requete = "http://localhost:81/moocMobile/selectCoursParId.php?id"
                        + "=" + id_cours;
                try {
                    CoursHandler offreHandler = new CoursHandler();
                    SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
                    HttpConnection hc = (HttpConnection) Connector.open(requete);
                    DataInputStream dis = new DataInputStream(hc.openDataInputStream());
                    parser.parse(dis, offreHandler);
                    offre_table = offreHandler.getCours();
                } catch (Exception e) {
                    System.out.println("Exception:" + e.toString());
                }
                currentChapitre.setCour(offre_table[0]);

            }
        }

    }
}
