/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.HANDLER;

import java.util.Vector;
import mooc.ENTITIES.Cours;
import mooc.ENTITIES.SuivCours;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Anouar
 */
public class SuivCoursHandler extends DefaultHandler {

    private Vector peopleVector;

    public SuivCoursHandler() {
        peopleVector = new Vector();
    }

    public SuivCours[] getPeople() {
        SuivCours[] suivcourTab = new SuivCours[peopleVector.size()];
        peopleVector.copyInto(suivcourTab);
        return suivcourTab;
    }
    private SuivCours suivCours;

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("SuivIdUtilisateur")) {
            
            String cour = attributes.getValue("cours_id");
            String utilisateur = attributes.getValue("utilisateur");
            String evaluation = attributes.getValue("evaluation");
            String badge = attributes.getValue("badge");
            int idcours = Integer.parseInt(cour);
            int idutilisateur = Integer.parseInt(utilisateur);

            if (cour == null || utilisateur == null || evaluation == null || badge == null) {
                throw new IllegalArgumentException("Person requires both ");
            }

        }
    }
 public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("SuivIdUtilisateur")) {
            // add completed Person object to collection
             peopleVector.addElement(suivCours);
            
            // we are no longer processing a <person.../> tag
            suivCours= null;
        } 
    }
 public void characters(char[] ch, int start, int length) throws SAXException {
      
    }

}
