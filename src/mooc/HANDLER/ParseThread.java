/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mooc.HANDLER;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import mooc.ENTITIES.News;
import mooc.GUI.RSS;
import org.kxml2.io.KXmlParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/**
 *
 * @author Bali Majdi
 */
public class ParseThread {

    
        protected RSS parentMidlet;

    public ParseThread(RSS parent) {
        parentMidlet = parent;
    }

    //method that run on a secondary thread for getting the XML
public void getXMLFeed(final String url) {
    Thread t = new Thread() {
        public void run() {
            HttpConnection myConnection = null;
            try {
                //open the connection
                myConnection = (HttpConnection) Connector.open(url);

//                    // Set the request method and headers
//                    myConnection.setRequestMethod(HttpConnection.GET);
//                    myConnection.setRequestProperty("Content-Type", "//text plain");
//                    myConnection.setRequestProperty("Connection", "close");
//
//                    //check the result
//                    int rc = myConnection.getResponseCode();
//                    if (rc != HttpConnection.HTTP_OK) {
//                        throw new IOException("HTTP response code: " + rc);
//                    }
                //get the bytes stream
                InputStream stream = myConnection.openInputStream();
                //call the parsing method
                ParseXMLFeed(stream);
            } catch (Exception error) {
                parentMidlet.DisplayError(error);
            } finally {
                //close the connection
                try {
                    if (myConnection != null) {
                        myConnection.close();
                    }
                } catch (IOException eroareInchidere) {
                    //calls a method from the MIDlet that display an alert
                    parentMidlet.DisplayError(eroareInchidere);
                }
            }
        }
    };
    //starts the thread
    t.start();
}

    private void ParseXMLFeed(InputStream input)
            throws IOException, XmlPullParserException {
        Reader dataReader = new InputStreamReader(input);
        
        //kXML parser
        KXmlParser myParser = new KXmlParser();
        myParser.setInput(dataReader);

        //skip the first node - rss
        myParser.nextTag();
        myParser.require(XmlPullParser.START_TAG, null, "rss");
        //skip the second node - channel
        myParser.nextTag();
        myParser.require(XmlPullParser.START_TAG, null, "channel");
        myParser.nextTag();
        myParser.require(XmlPullParser.START_TAG, null, "title");

        while (myParser.getEventType() != XmlPullParser.END_DOCUMENT) {
            String name = myParser.getName();
            //this is for reaching the end tag of channel node
            if (name.equals("channel")) {
                break;
            }
            //this is for each item node in the RSS feed
            if (name.equals("item")) {
                //process it only if it is reached the start tag
                if (myParser.getEventType() != XmlPullParser.END_TAG) {
                    myParser.nextTag();
                    String title = myParser.nextText();
                    myParser.nextTag();
                    String link = myParser.nextText();

                    //create a new News item
                    News news = new News(title, link);
                    parentMidlet.addNews(news);
                }
            } else {
                //if it is not the item node the skip it and all its details
                myParser.skipSubTree();
            }
            //move to the next tag
            myParser.nextTag();
        }
        //close the input stream - job done
        input.close();
    }
}
