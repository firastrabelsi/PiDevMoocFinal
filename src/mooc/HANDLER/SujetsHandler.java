/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.HANDLER;

/**
 *
 * @author Bali Majdi
 */

import java.util.Vector;
import mooc.ENTITIES.Sujet;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class SujetsHandler extends DefaultHandler{

    private Vector sujets;
    String idTag = "close";
    String sujetTag = "close";
    String date_pubTag = "close";
    String id_userTag = "close";
    String categorieTag = "close";
    String texteTag = "close";

    public SujetsHandler() {
        sujets = new Vector();
    }

    public Sujet[] getSujet() {
        Sujet[] sujetss = new Sujet[sujets.size()];
        sujets.copyInto(sujetss);
        return sujetss;
    }
    // VARIABLES TO MAINTAIN THE PARSER'S STATE DURING PROCESSING
    private Sujet currentSujet;

    // XML EVENT PROCESSING METHODS (DEFINED BY DefaultHandler)
    // startElement is the opening part of the tag "<tagname...>"
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("sujet")) {
            currentSujet = new Sujet();
        } else if (qName.equals("id")) {
            idTag = "open";
        } else if (qName.equals("sujett")) {
            sujetTag = "open";
        } else if (qName.equals("date_pub")) {
            date_pubTag = "open";
        } else if (qName.equals("id_user")) {
            id_userTag = "open";
        } else if (qName.equals("categorie")) {
            categorieTag = "open";
        }  else if (qName.equals("texte")) {
            texteTag = "open";
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("sujet")) {
            // we are no longer processing a <reg.../> tag
            sujets.addElement(currentSujet);
            currentSujet = null;
        } else if (qName.equals("id")) {
            idTag = "close";
        } else if (qName.equals("sujett")) {
            sujetTag = "close";
        } else if (qName.equals("date_pub")) {
            date_pubTag = "close";
        } else if (qName.equals("id_user")) {
            id_userTag = "close";
        } else if (qName.equals("categorie")) {
            categorieTag = "close";
        } else if (qName.equals("texte")) {
            texteTag = "close";
        }

    }
    // "characters" are the text between tags

    public void characters(char[] ch, int start, int length) throws SAXException {
        // we're only interested in this inside a <phone.../> tag
        if (currentSujet != null) {
            // don't forget to trim excess spaces from the ends of the string
            if (idTag.equals("open")) {
                String id = new String(ch, start, length).trim();
                currentSujet.setId(Integer.parseInt(id));
            } else if (sujetTag.equals("open")) {
                String sujet = new String(ch, start, length).trim();
                currentSujet.setSujet(sujet);
            } else if (date_pubTag.equals("open")) {
                String date_pub = new String(ch, start, length).trim();
                currentSujet.setDate_pub(date_pub);
            } else if (id_userTag.equals("open")) {
                String id_user = new String(ch, start, length).trim();
                currentSujet.setId_user(Integer.parseInt(id_user));
            } else if (categorieTag.equals("open")) {
                String categorie = new String(ch, start, length).trim();
                currentSujet.setCategorie(categorie);
               
            } else if (texteTag.equals("open")) {
                String texte = new String(ch, start, length).trim();
                currentSujet.setTexte(texte);
        }
         System.out.println(" ======= "+ currentSujet.toString());
    }

}
}

