/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.HANDLER;

import mooc.HANDLER.*;
import mooc.ENTITIES.Formation;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Dell
 */
public class FormationHandler extends DefaultHandler {

    private Vector formation;
    String nomFormTag = "close";
    String dureeTag = "close";
    String levelTag = "close";
    String IdFormTag = "close";

    public FormationHandler() {
        formation = new Vector();
    }

    public Formation[] getFormation() {
        Formation[] formationn = new Formation[formation.size()];
        formation.copyInto(formationn);
        return formationn;
    }
    // VARIABLES TO MAINTAIN THE PARSER'S STATE DURING PROCESSING
    private Formation currentFormation;

    ////////////////////////////////
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("mydata")) {

            if (currentFormation != null) {
                throw new IllegalStateException("already processing a formation");
            }
            currentFormation = new Formation();
        } else if (qName.equals("nomForm")) {
            nomFormTag = "open";
        } else if (qName.equals("duree")) {
            dureeTag = "open";
        } else if (qName.equals("level")) {
            levelTag = "open";
        } else if (qName.equals("IdForm")) {
            IdFormTag = "open";
        }
    }

    ////////////////////////////////////////

    public void endElement(String uri, String localName, String qName) throws SAXException {

        if (qName.equals("mydata")) {
            // we are no longer processing a <reg.../> tag
            formation.addElement(currentFormation);
            currentFormation = null;
        } else if (qName.equals("nomForm")) {
            nomFormTag = "close";
        } else if (qName.equals("duree")) {
            dureeTag = "close";
        } else if (qName.equals("level")) {
            levelTag = "close";
        } else if (qName.equals("IdForm")) {
            IdFormTag = "close";
        }
    }

    ///////////////////////////////////////////

    public void characters(char[] ch, int start, int length) throws SAXException {
        // we're only interested in this inside a <phone.../> tag
        if (currentFormation != null) {
            // don't forget to trim excess spaces from the ends of the string
            if (nomFormTag.equals("open")) {
                String nomForm = new String(ch, start, length).trim();
                currentFormation.setNomForm(nomForm);
            } else if (dureeTag.equals("open")) {
                int duree = Integer.parseInt(new String(ch, start, length).trim());
                currentFormation.setDuree(duree);
            } else if (levelTag.equals("open")) {
                String level = new String(ch, start, length).trim();
                currentFormation.setLevel(level);
            } else if (IdFormTag.equals("open")) {
                int IdForm = Integer.parseInt(new String(ch, start, length).trim());
                currentFormation.setIdForm(IdForm);
            }
        }
    
} 
}
