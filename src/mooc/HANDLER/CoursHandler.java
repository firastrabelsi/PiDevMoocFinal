/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.HANDLER;

import mooc.ENTITIES.Cours;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Dell
 */
public class CoursHandler extends DefaultHandler {

    private Vector cours;
     String idCoursTag = "close";
    String titreTag = "close";
    String descriptionTag = "close";
    String etatTag = "close";
    String DateDebutTag = "close";
    String DateFinTag = "close";
    String difficulteTag = "close";
    String formationTag = "close";

    public CoursHandler() {
        cours = new Vector();
    }

    public Cours[] getCours() {
        Cours[] formationn = new Cours[cours.size()];
        cours.copyInto(formationn);
        return formationn;
    }
    // VARIABLES TO MAINTAIN THE PARSER'S STATE DURING PROCESSING
    private Cours currentCours;

    ////////////////////////////////

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("mydata")) {

            if (currentCours != null) {
                throw new IllegalStateException("already processing a formation");
            }
            currentCours = new Cours();
            
        } else if (qName.equals("id")) {
            idCoursTag = "open";
        } else if (qName.equals("titre")) {
            titreTag = "open";
        } else if (qName.equals("description")) {
            descriptionTag = "open";
        } else if (qName.equals("etat")) {
            etatTag = "open";
         
            DateFinTag = "open";
        } else if (qName.equals("difficulte")) {
            difficulteTag = "open";
        } else if (qName.equals("formation")) {
            formationTag = "open";
        }
    } 
     ////////////////////////////////////////

    public void endElement(String uri, String localName, String qName) throws SAXException {

        if (qName.equals("mydata")) {
            // we are no longer processing a <reg.../> tag
            cours.addElement(currentCours);
            currentCours = null;
            } else if (qName.equals("id")) {
            idCoursTag = "close";
        } else if (qName.equals("titre")) {
            titreTag = "close";
        } else if (qName.equals("description")) {
            descriptionTag = "close";
        } else if (qName.equals("etat")) {
            etatTag = "close";
        
        } else if (qName.equals("difficulte")) {
            difficulteTag = "close";
        } else if (qName.equals("formationt")) {
            formationTag = "close";
        }
    }

    ///////////////////////////////////////////
    public void characters(char[] ch, int start, int length) throws SAXException {
        // we're only interested in this inside a <phone.../> tag
        try {
        if (currentCours != null) {
            // don't forget to trim excess spaces from the ends of the string
            
             String value = new String(ch, start, length).trim();
             if (idCoursTag.equals("open")) {
                int idCours = Integer.parseInt(new String(ch, start, length).trim());
                currentCours.setId(idCours);
            }
            if (titreTag.equals("open")) {
                String titre = new String(ch, start, length).trim();
                currentCours.setTitre(titre);
            } else if (descriptionTag.equals("open")) {
                String description = new String(ch, start, length).trim();
                currentCours.setDescription(description);
            } else if (etatTag.equals("open")) {
                 String etat = new String(ch, start, length).trim();
                currentCours.setEtat(etat);
            
          /*  } else if (DateDebutTag.equals("open")) {
                    String[] strDate  = split(value, "-");
                    System.out.println(""+value);
                    Date d = new Date();
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, Integer.parseInt(strDate[0]));
                    calendar.set(Calendar.MONTH, Integer.parseInt(strDate[1]));
                    calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(strDate[2]));
                   d.setTime(calendar.getTime().getTime());
                    currentCours.setDateDebut(d);
                } 
             else if (DateFinTag.equals("open")) {
                    String[] strDate  = split(value, "-");
                    Date d = new Date();
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, Integer.parseInt(strDate[0]));
                    calendar.set(Calendar.MONTH, Integer.parseInt(strDate[1]));
                    calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(strDate[2]));
                   d.setTime(calendar.getTime().getTime());
                    currentCours.setDateFin(d);
                }*/
            } else if (difficulteTag.equals("open")) {
               String description = new String(ch, start, length).trim();
                currentCours.setDescription(description);
            } 
        }
        } catch (Exception e) {
        }
         //else if (formationTag.equals("open")) {
             //   int formation = Integer.parseInt(new String(ch, start, length).trim());
            //    currentCours.setFormation(null);
           // }
    }
      private String[] split(String original,String separator) {
    Vector nodes = new Vector();
    // Parse nodes into vector
    int index = original.indexOf(separator);
    while(index >= 0) {
        nodes.addElement( original.substring(0, index) );
        original = original.substring(index+separator.length());
        index = original.indexOf(separator);
    }
    // Get the last node
    nodes.addElement( original );

     // Create split string array
    String[] result = new String[ nodes.size() ];
    if( nodes.size() > 0 ) {
        for(int loop = 0; loop < nodes.size(); loop++)
        {
            result[loop] = (String)nodes.elementAt(loop);
            System.out.println(result[loop]);
        }

    }
   return result;
}
} 

