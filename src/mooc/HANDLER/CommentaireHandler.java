/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.HANDLER;

/**
 *
 * @author Bali Majdi
 */
import mooc.ENTITIES.Commentaire;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class CommentaireHandler extends DefaultHandler{

    private Vector commentaires;
    String idTag = "close";
    String personneIdTag = "close";
    String coursIdTag = "close";
    String contenuTag = "close";
    String dateTag = "close";

    public CommentaireHandler() {
        commentaires = new Vector();
    }

    public Commentaire[] getCommentaire() {
        Commentaire[] commentairess = new Commentaire[commentaires.size()];
        commentaires.copyInto(commentairess);
        return commentairess;
    }
    // VARIABLES TO MAINTAIN THE PARSER'S STATE DURING PROCESSING
    private Commentaire currentCommentaire;

    // XML EVENT PROCESSING METHODS (DEFINED BY DefaultHandler)
    // startElement is the opening part of the tag "<tagname...>"
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("commentaire")) {
            currentCommentaire = new Commentaire();
        } else if (qName.equals("id")) {
            idTag = "open";
        } else if (qName.equals("personne_id")) {
            personneIdTag = "open";
        } else if (qName.equals("cours_id")) {
            coursIdTag = "open";
        } else if (qName.equals("contenu")) {
            contenuTag = "open";
        } else if (qName.equals("date")) {
            dateTag = "open";
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("commentaire")) {
            // we are no longer processing a <reg.../> tag
            commentaires.addElement(currentCommentaire);
            currentCommentaire = null;
        } else if (qName.equals("id")) {
            idTag = "close";
        } else if (qName.equals("personne_id")) {
            personneIdTag = "close";
        } else if (qName.equals("cours_id")) {
            coursIdTag = "close";
        } else if (qName.equals("contenu")) {
            contenuTag = "close";
        } else if (qName.equals("date")) {
            dateTag = "close";
        }

    }
    // "characters" are the text between tags

    public void characters(char[] ch, int start, int length) throws SAXException {
        // we're only interested in this inside a <phone.../> tag
        if (currentCommentaire != null) {
            // don't forget to trim excess spaces from the ends of the string
            if (idTag.equals("open")) {
                String id = new String(ch, start, length).trim();
                currentCommentaire.setId(Integer.parseInt(id));
            } else if (personneIdTag.equals("open")) {
                String pr = new String(ch, start, length).trim();
                currentCommentaire.setUt(Integer.parseInt(pr));
            } else if (coursIdTag.equals("open")) {
                String cr = new String(ch, start, length).trim();
                currentCommentaire.setCours(Integer.parseInt(cr));
            } else if (contenuTag.equals("open")) {
                String cnt = new String(ch, start, length).trim();
                currentCommentaire.setContenu(cnt);
            } else if (dateTag.equals("open")) {
                String date = new String(ch, start, length).trim();
                currentCommentaire.setDateS(date);
            }
        }
        System.out.println(" chouf "+ currentCommentaire.toString());
    }

}

