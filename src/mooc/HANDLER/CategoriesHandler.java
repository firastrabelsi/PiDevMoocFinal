/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mooc.HANDLER;

import java.util.Vector;
import mooc.ENTITIES.Categories;
import mooc.ENTITIES.Cours;
import mooc.ENTITIES.SuivCours;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author Anouar
 */
public class CategoriesHandler extends DefaultHandler{
    private Vector categVector;
public CategoriesHandler() {
        categVector = new Vector();
    }
    public Categories[] getCategories() {
        Categories[] categoriesTab = new Categories[categVector.size()];
        categVector.copyInto(categoriesTab);
        return categoriesTab;
    }
private     Categories currentcategorie ;
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equals("categ")) {
            String idcat = attributes.getValue("id");
            String cate = attributes.getValue("cate");
            int id=Integer.parseInt(idcat);
            currentcategorie = new Categories(id,cate);


            if (cate == null) {
                throw new IllegalArgumentException("Categories requires both ");
            }

        }}
 public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("categ")) {
            // add completed Person object to collection
             categVector.addElement(currentcategorie);
            
            // we are no longer processing a <person.../> tag
            currentcategorie= null;
        } 
    }
 public void characters(char[] ch, int start, int length) throws SAXException {
      
    }

    }


