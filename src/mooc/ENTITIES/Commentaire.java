/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.ENTITIES;

import java.util.Date;

/**
 *
 * @author Bali Majdi
 */
public class Commentaire {
    
    private int id;
    private String contenu;
    private Date date;
    private int cours;
    private int ut;
    private String dateS;

    public String getDateS() {
        return dateS;
    }

    public void setDateS(String dateS) {
        this.dateS = dateS;
    }

    public Commentaire(int id, String contenu, Date date, int cours, int ut) {
        this.id = id;
        this.contenu = contenu;
        this.date = date;
        this.cours = cours;
        this.ut = ut;
    }
        public Commentaire(String contenu, int cours, int ut) {
       
        this.contenu = contenu;
        
        this.cours = cours;
        this.ut = ut;
    }

 public Commentaire(int id,String contenu) {
       this.id = id;
        this.contenu = contenu;       
}
 
 public Commentaire(String contenu, Date date ,int ut ) {
        this.ut = ut;
         this.date = date;
        this.contenu = contenu;       
}
 
 
 
  public Commentaire() {
       
}
    public int getCours() {
        return cours;
    }

    public void setCours(int cours) {
        this.cours = cours;
    }

    public int getUt() {
        return ut;
    }

    public void setUt(int ut) {
        this.ut = ut;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String toString() {
        return contenu+"\n"+" "+dateS;
    }
}
