package mooc.ENTITIES;



public class Cours {

    private int id;
    private String titre;
    private String description;
    private int dureedecours;
    private String difficulte;
    private String etat;
    private int categorie;
    private int formateur;
    private int appren;
    private int organi;
    private int vedio_id;

    public Cours() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDureedecours() {
        return dureedecours;
    }

    public void setDureedecours(int dureedecours) {
        this.dureedecours = dureedecours;
    }

    public String getDifficulte() {
        return difficulte;
    }

    public void setDifficulte(String difficulte) {
        this.difficulte = difficulte;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public int getCategorie() {
        return categorie;
    }

    public void setCategorie(int categorie) {
        this.categorie = categorie;
    }

    public int getFormateur() {
        return formateur;
    }

    public void setFormateur(int formateur) {
        this.formateur = formateur;
    }

    public int getAppren() {
        return appren;
    }

    public void setAppren(int appren) {
        this.appren = appren;
    }

    public int getOrgani() {
        return organi;
    }

    public void setOrgani(int organi) {
        this.organi = organi;
    }

    public int getVedio_id() {
        return vedio_id;
    }

    public void setVedio_id(int vedio_id) {
        this.vedio_id = vedio_id;
    }

    public Cours(String titre, String description, int dureedecours, String difficulte, int categorie, int formateur, int organi) {
        this.titre = titre;
        this.description = description;
        this.dureedecours = dureedecours;
        this.difficulte = difficulte;
        this.categorie = categorie;
        this.formateur = formateur;
        this.organi = organi;
    }

    public Cours(String titre, String description, String difficulte) {
        this.titre = titre;
        this.description = description;
        this.difficulte = difficulte;
    }

    public String toString() {
        return "Cours:" + "titre=" + titre + ", dureedecours=" + dureedecours ;
    }


}
