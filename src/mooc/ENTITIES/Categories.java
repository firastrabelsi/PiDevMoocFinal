

package mooc.ENTITIES;


public class Categories {
    private int  id;
    private String cate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCate() {
        return cate;
    }

    public Categories(int id, String cate) {
        this.id = id;
        this.cate = cate;
    }

    public void setCate(String cate) {
        this.cate = cate;
    }

    public Categories() {
    }

    public String toString() {
        return "Categories : \n \n" + " id : " + id + "       Type : " + cate + '\n';
    }

    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        hash = 97 * hash + (this.cate != null ? this.cate.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Categories other = (Categories) obj;
        if (this.id != other.id) {
            return false;
        }
        if ((this.cate == null) ? (other.cate != null) : !this.cate.equals(other.cate)) {
            return false;
        }
        return true;
    }

 

    
}
