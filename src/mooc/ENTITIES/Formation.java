/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.ENTITIES;

/**
 *
 * @author Dell
 */
public class Formation {

    private String nomForm;
    private int duree;
    private String level;
    private int IdForm;

    public Formation() {
    }

    public Formation(String nomForm, int duree, String level) {
        this.nomForm = nomForm;
        this.duree = duree;
        this.level = level;
    }
public String getFullInfo() {
        return nomForm + ", " + duree + ", " +level;
    }
    public Formation(String nomForm, int duree, String level, int IdForm) {
        this.nomForm = nomForm;
        this.duree = duree;
        this.level = level;
        this.IdForm = IdForm;
    }

    public String getNomForm() {
        return nomForm;
    }

    public void setNomForm(String nomForm) {
        this.nomForm = nomForm;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getIdForm() {
        return IdForm;
    }

    public void setIdForm(int IdForm) {
        this.IdForm = IdForm;
    }
    
}
