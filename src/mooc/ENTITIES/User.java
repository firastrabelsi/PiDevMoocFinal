/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.ENTITIES;

/**
 *
 * @author Firas
 */
public class User {
    private int id;
    private String username;
    private String password;
    private String email;
    private String nom;
    private String prenom;
    private String role;
    private String etat;
    private String adresse;
    private String url;
    private String nomsociete;
    private int num;
    private String specialite;
    private String site;
    private String certificat;
    private int etat_c;

    public int getEtat_c() {
        return etat_c;
    }

    public void setEtat_c(int etat_c) {
        this.etat_c = etat_c;
    }
    
    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }
   
 //aprenant   

    public User(int id, String username, String password, String email, String nom, String prenom) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public String getCertificat() {
        return certificat;
    }

    public void setCertificat(String certificat) {
        this.certificat = certificat;
    }
//organisme
    public User(int id,String adresse, String nomsociete, int num, String site, String certificat , String username, String password) {
        this.adresse = adresse;
        this.nomsociete = nomsociete;
        this.num = num;
        this.site = site;
        this.certificat = certificat;
        this.password = password;
        this.email = email;
        this.id = id;
    }

  //formateur

    public User(int id, String username, String password, String email, String nom, String prenom, String specialite) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.nom = nom;
        this.prenom = prenom;
        this.specialite = specialite;
    }
    




    public User() {
       
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNomsociete() {
        return nomsociete;
    }

    public void setNomsociete(String nomsociete) {
        this.nomsociete = nomsociete;
    }

    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", password=" + password + ", email=" + email + ", nom=" + nom + ", prenom=" + prenom + ", role=" + role + ", etat=" + etat + ", adresse=" + adresse + ", url=" + url + ", nomsociete=" + nomsociete + '}';
    }

   
    
    
    
    
    
}
