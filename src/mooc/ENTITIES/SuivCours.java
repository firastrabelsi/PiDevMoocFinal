/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mooc.ENTITIES;

/**
 *
 * @author Anouar
 */
public class SuivCours {
    private int id;
    private Cours cours;
    private int apprenant;
    private int evaluation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cours getCours() {
        return cours;
    }

    public void setCours(Cours cours) {
        this.cours = cours;
    }

    public int getApprenant() {
        return apprenant;
    }

    public void setApprenant(int apprenant) {
        this.apprenant = apprenant;
    }

    public int getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(int evaluation) {
        this.evaluation = evaluation;
    }

    public SuivCours() {
    }

    public SuivCours(int id, Cours cours, int apprenant) {
        this.id = id;
        this.cours = cours;
        this.apprenant = apprenant;
    }


}
