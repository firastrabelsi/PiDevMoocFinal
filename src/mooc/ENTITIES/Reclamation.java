/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mooc.ENTITIES;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Firas
 */
public class Reclamation {
    private int id;
    private String categorie;
    private String text;
    private String email;
    private String date;
    private String etat;
    private int tel;
    private int iduser;

    public Reclamation() {

        this.date=getSys();
        this.etat="Attente";
    }
public String getSys(){
    String stDate = null;
    String dd = null;
    String mm = null;
    String yyyy = null;
    Calendar cd = Calendar.getInstance();
    int iD = cd.get( Calendar.DAY_OF_MONTH );
    if (iD < 10){ dd = "0" + String.valueOf(iD); }
        else { dd = String.valueOf(iD); }
    int iM = cd.get( Calendar.MONTH )+1;
        if (iM < 10){ mm = "0" + String.valueOf(iM); }
        else { mm = String.valueOf(iM); }
    int iY = cd.get( Calendar.YEAR );
        yyyy = String.valueOf(iY);
    stDate = yyyy+"-"+mm+"-"+dd;
    return stDate;
}
    public Reclamation(int id, String categorie, String text, String email, String date, String etat,int tel, int iduser) {
        this.id = id;
        this.categorie = categorie;
        this.text = text;
        this.email = email;
        Date date1 = new Date();
        this.date=date1.toString();
        this.etat = "Attente";
        this.tel = tel;
        this.iduser = iduser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public String toString() {
        return "Reclamation{" + "id=" + id + ", categorie=" + categorie + ", text=" + text + ", email=" + email + ", date=" + date + ", etat=" + etat + ", iduser=" + iduser + '}';
    }

    public int getTel() {
        return tel;
    }

    public void setTel(int tel) {
        this.tel = tel;
    }
    
    
    
    
    

}
