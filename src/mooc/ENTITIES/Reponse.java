/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mooc.ENTITIES;

/**
 *
 * @author Aymen
 */
public class Reponse {
    private int id;
    private String date;
    private String texte;
    private int id_user;
    private int id_sujet;
    
    public Reponse(){
    
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_sujet() {
        return id_sujet;
    }

    public void setId_sujet(int id_sujet) {
        this.id_sujet = id_sujet;
    }

    public String toString() {
        return "Reponse{" + "id=" + id + ", date=" + date + ", texte=" + texte + ", id_user=" + id_user + ", id_sujet=" + id_sujet + '}';
    }
    
    
    
}
