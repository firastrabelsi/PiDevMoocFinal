/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mooc.ENTITIES;

import java.util.Date;

/**
 *
 * @author Aymen
 */
public class Sujet {
    private int id;
    private String sujet ;
    private String date_pub;
    private int id_user ;
    private String categorie;
    private String texte;

    public Sujet() {
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public String getDate_pub() {
        return date_pub;
    }

    public void setDate_pub(String date_pub) {
        this.date_pub = date_pub;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String toString() {
        return "Sujet{" + "id=" + id + ", sujet=" + sujet + ", date_pub=" + date_pub + ", id_user=" + id_user + ", categorie=" + categorie + ", texte=" + texte + '}';
    }

    
    
    
    

    public void setId(String id) {
        this.id= Integer.parseInt(id);
    }

    public void setId_user(String id_user) {
        this.id_user = Integer.parseInt(id_user);

    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }
    
    
    
}
