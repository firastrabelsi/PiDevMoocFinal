/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mooc.MIDLET;

import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author Firas
 */
public class Splashscreen extends Canvas implements Runnable {

        private Image mImage;

        public Splashscreen() {

            try {
                mImage = Image.createImage("/54396.jpg");
                Thread t = new Thread(this);
                t.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void paint(Graphics g) {
            int width = getWidth();
            int height = getHeight();
            //set background color to overdraw what ever was previously displayed
            g.setColor(0xFFFFFF);
            g.fillRect(0, 0, width, height);
            g.drawImage(mImage, width / 2, height / 2, Graphics.HCENTER | Graphics.VCENTER);
        }

        public void dismiss() {
            if (isShown()) {
                //dis.setCurrent(new Principale("Inscription en tant que ?", List.IMPLICIT, dis));
               //new Test(dis);
                //new Authentification(dis);
            }

        }

        public void run() {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                System.out.println("InterruptedException");
                e.printStackTrace();
            }
            dismiss();
        }

        public void keyReleased(int keyCode) {
            dismiss();
        }

        public void pointerReleased(int x, int y) {
            dismiss();
        }
    }


