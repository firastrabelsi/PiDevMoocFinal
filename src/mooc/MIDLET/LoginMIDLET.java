/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.MIDLET;

import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.ImageItem;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;
import javax.microedition.midlet.*;
import mooc.ENTITIES.User;
import mooc.GUI.Accueil;
import mooc.GUI.ListUser;
import mooc.GUI.Reclamer;
import mooc.GUI.RegistrationChoix;
import mooc.GUI.login;
import javax.microedition.lcdui.Alert;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;
import javax.microedition.media.control.VideoControl;
import mooc.GUI.ListReclamation;
import mooc.GUI.ListeApprenant;
import mooc.GUI.ListeCommentaire;
import mooc.GUI.ListeFormateur;
import mooc.GUI.ListeOrganismeNonValider;
import mooc.GUI.ListeOrganismeValide;
import mooc.GUI.ListeS;
import mooc.GUI.MenuAdmin;
import mooc.GUI.MenuApprenant;
import mooc.GUI.MenuComite;
import mooc.GUI.MenuFormateur;
import mooc.GUI.MenuOrganisme;
import mooc.GUI.MenuValiderOrganisme;
import mooc.GUI.MenuValiderReclamation;
import mooc.GUI.Sendsms;

/**
 * @author Firas
 */
public class LoginMIDLET extends MIDlet implements CommandListener, Runnable  {
    public static User currentsession;

//    Splashscreen splashscreen = new Splashscreen();
 //   Accueil acceuil = new Accueil();
    
    /**
     *
     */
    public Display display = Display.getDisplay(this);
    public static LoginMIDLET mMidlet;
    
    Player player = null;

    private boolean error = false;
    private Canvas canvas = new VideoCanvas();

    public LoginMIDLET() {
        display = Display.getDisplay(this);

    }

    public void startApp() {
        mMidlet=this;
        if (error) {
            return;
        }
        try {
            loadPlayer();

            VideoControl videoControl = (VideoControl) player.getControl("javax.microedition.media.control.VideoControl");
            if (videoControl == null) {
                throw new Exception("No VideoControl!!");
            }
            videoControl.initDisplayMode(VideoControl.USE_DIRECT_VIDEO, canvas);
            videoControl.setDisplayFullScreen(true);
            videoControl.setVisible(true);
            display.setCurrent(canvas);
            player.start();

        } catch (Exception e) {
        }
        Thread t = new Thread((Runnable) this);
        display.setCurrent(canvas);
        try {
            t.sleep(2000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        edu.esprit.utils.StaticMidlet.mainMid = this;
       edu.esprit.utils.StaticMidlet.disp = display;
       // display.setCurrent(new ListeS("aaa", List.IMPLICIT, display));
       display.setCurrent(new Accueil());
       // display.setCurrent(new ListeCommentaire("Commentaires", List.IMPLICIT, display));
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
        try {
            if (player != null) {
                player.close();
            }
        } catch (Exception e) {
        }
    }

    public void commandAction(Command cmd, Displayable disp) {

    }

    private void loadPlayer() throws Exception {
        player = Manager.createPlayer(getClass().getResourceAsStream("/mooc/IMAGE/loader.gif"), "image/gif");
        player.realize();
    }

    

    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


class VideoCanvas extends Canvas {

    public void paint(Graphics g) {
    }
}
}