/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import mooc.ENTITIES.Categories;
import mooc.ENTITIES.Cours;
import mooc.HANDLER.CategoriesHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author Anouar
 */
public class ListouCoursValider extends List implements CommandListener, Runnable {

    Command cmdsup = new Command("Supprimer", Command.SCREEN, 0);
    Command cmdretour = new Command("Retour", Command.SCREEN, 1);

    Cours[] tcrs;
    Categories[] tcategs;

    Display disp;

    public ListouCoursValider(String title, int listType, Display d) {
        super(title, listType);
        disp = d;
        Thread th = new Thread(this);
        th.start();

    }

    public void commandAction(Command c, Displayable d) {
    if (c == List.SELECT_COMMAND) {

            AfficherCoursValider.coursvalid = tcrs[this.getSelectedIndex()];
            Categories categ = selectbyidCategories(tcrs[this.getSelectedIndex()].getCategorie());
            AfficherCoursValider form = new AfficherCoursValider("Cours validé", disp);
            form.append("Formateur: " + tcrs[this.getSelectedIndex()].getFormateur() + "        Categories: " + categ.getCate() + "\n");
            form.append("                      Titre:" + tcrs[this.getSelectedIndex()].getTitre() + "\n");
            form.append("Difficulte: " + tcrs[this.getSelectedIndex()].getDifficulte() + "               Duree: " + tcrs[this.getSelectedIndex()].getDureedecours() + "\n");
            form.append("Description: \n");
            form.append(tcrs[this.getSelectedIndex()].getDescription());

            disp.setCurrent(form);

//            System.out.println(this.getSelectedIndex());
//System.out.println(people[this.getSelectedIndex()].getTitre());
        }
    if (c == cmdretour) {
           disp.setCurrent(new Menu(disp));
        }
     
    }

    public void run() {
        try {
            setCommandListener(this);
            addCommand(cmdsup);
            addCommand(cmdretour);
            
            CoursHandler coursHandler = new CoursHandler();
            // get a parser object
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/lisCoursValider.php");//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            parser.parse(dis, coursHandler);
            // display the result
            tcrs = coursHandler.getCours();
            // System.out.println("++++++++++++++"+people.length);

            if (tcrs.length > 0) {
                for (int i = 0; i < tcrs.length; i++) {
                    append(tcrs[i].getTitre(), null);
                }
            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
  public Categories selectbyidCategories(int idcateG) {
        Categories categ = new Categories();
        try {
            CategoriesHandler categoriesHandler = new CategoriesHandler();
            // get a parser object
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            String urle = "http://localhost/MoocPHPJ2ME/getCategoriesbyid.php?idcat=" + idcateG;
            System.out.println(urle);
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open(urle);//people.xml est

            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            parser.parse(dis, categoriesHandler);
            // display the result
            tcategs = categoriesHandler.getCategories();
            // System.out.println("++++++++++++++"+people.length);

            if (tcategs.length > 0) {
                for (int i = 0; i < tcategs.length; i++) {
                    // append(tcategs[i].toString(), null);
                    categ = tcategs[i];
                }
                System.out.println(categ.getCate());
                return categ;

            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

}
