/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import mooc.GUI.AjouterChapitre;
import mooc.GUI.ListFormation;
import mooc.ENTITIES.Cours;
import mooc.HANDLER.CoursHandler;
import java.io.DataInputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 *
 * @author Dell
 */
public class ListCours extends List implements CommandListener, Runnable {

    String[] listData;
    Cours[] CoursData;
    Command backCmd = new Command("Retour", Command.EXIT, 0);
    Command ajouterChapitreCmd = new Command("Ajouter Chapitre", Command.SCREEN, 4);
    Thread t;

    int id;
    String label = "";
    boolean modifyFlag = false;

    public ListCours() {
        super("Cours", IMPLICIT);

        addCommand(backCmd);
        addCommand(ajouterChapitreCmd);
        setCommandListener(this);
        t = new Thread(this);
        t.start();
    }

    public ListCours(int id) {
        super("Cours", IMPLICIT);
        this.id = id;
        modifyFlag = true;
        addCommand(backCmd);
        addCommand(ajouterChapitreCmd);
        setCommandListener(this);
        t = new Thread(this);
        t.start();
    }

    public ListCours(String critere) {
        super("Cours", IMPLICIT);
        this.label = critere;
        addCommand(backCmd);
        addCommand(ajouterChapitreCmd);
        setCommandListener(this);
        t = new Thread(this);
        t.start();
    }

    private Cours[] setAllOffres() {
        Cours[] offre_table = null;
        String requete = "http://localhost/MoocPHPJ2ME/selectCours.php";

      /*  if (!label.equals("")) {
            System.out.println("Payslist : CRITERE_VIDE() " + !label.equals(""));
            requete = "http://localhost/pidev/SelectPaysParLabel.php?label=" + label;
            System.out.println("Payslist: requete affichage pays par label: " + requete);
        }   */
    try {
            CoursHandler offreHandler = new CoursHandler();
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            HttpConnection hc = (HttpConnection) Connector.open(requete);
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            parser.parse(dis, offreHandler);
            // display the result
             offre_table = offreHandler.getCours();
        } catch (Exception e) {
            System.out.println("Exception:" + e.toString());
        }
        return offre_table;
    }
    
    private void setListData()
    {
        CoursData = setAllOffres();
        
        if (CoursData!=null)
        {
           if (CoursData.length!=0)
           {
            listData = new String[CoursData.length];
            
            for (int i = 0; i < CoursData.length; i++) 
            {
                listData[i] =
                        "\n Id_cours    : "+CoursData[i].getId()
                       +"\n Titre : "+CoursData[i].getTitre(); 
            }
           }
        }
        else
        {
            append("Pas de Cours", null);
        }
    }
    public void commandAction(Command c, Displayable d) { 
        if (c ==backCmd)
        {
           edu.esprit.utils.StaticMidlet.disp.setCurrent(new ListFormation());
                        System.out.println(CoursData[this.getSelectedIndex()]);
        } 
        if (c == ajouterChapitreCmd){
            edu.esprit.utils.StaticMidlet.disp.setCurrent(new AjouterChapitre(CoursData[this.getSelectedIndex()]));
                        System.out.println(CoursData[this.getSelectedIndex()]);

            
        }
    }

    public void run() { 
        
        setListData();
        if (listData != null)
        {
            for (int i = 0; i < listData.length; i++) {
                append(listData[i], null);
//              append(critere, null);
            }
        }
        closeThread();
    }
    
    private void closeThread()
    {
        t.interrupt();
    }

}
