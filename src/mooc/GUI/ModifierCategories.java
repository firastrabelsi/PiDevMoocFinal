/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Anouar
 */
public class ModifierCategories extends Form implements CommandListener, Runnable {

    Display disp;
    TextField categorie = new TextField("Type Categories", "", 100, TextField.ANY);

    Command cmdmodifier = new Command("Modifier", Command.OK, 0);
    //conexion
    HttpConnection hc;
    DataInputStream dis;
    String url = "http://localhost/MoocPHPJ2ME/modifierCategories.php?";
    StringBuffer sb = new StringBuffer();
    int ch;

    public ModifierCategories(String title,Display disp) {
        super(title);
        this.disp = disp;
        categorie.setLayout(categorie.LAYOUT_BOTTOM | categorie.LAYOUT_CENTER);
        this.append(categorie);
        this.addCommand(cmdmodifier);
        setCommandListener(this);
        disp.setCurrent(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdmodifier) {
            try {
                System.out.println("******");
                url=url+"id=4&cat=" +categorie.getString().trim();
                 System.out.println(url);
                hc = (HttpConnection) Connector.open(url);
                        
                dis = new DataInputStream(hc.openDataInputStream());
                while ((ch = dis.read()) != -1) {
                    sb.append((char) ch);
                }
                 if ("OK".equals(sb.toString().trim())) {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.CONFIRMATION);
                    a.setTimeout(3000);
                    disp.setCurrent(a);
                } else {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    disp.setCurrent(a);
                }
                sb = new StringBuffer();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
