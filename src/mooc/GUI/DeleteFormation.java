/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import mooc.ENTITIES.Formation;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.ItemStateListener;

/**
 *
 * @author Dell
 */
public class DeleteFormation extends Form implements CommandListener, Runnable, ItemStateListener {

    Formation formation_instance;
    HttpConnection http;
    DataInputStream dataStream;
    Thread t;

    public DeleteFormation(Formation formation_instance) {
        super("Supprimer formation");
        this.formation_instance = formation_instance;

        t = new Thread(this);
        t.start();
        this.setItemStateListener(this);
        this.setCommandListener(this);

    }

    public void commandAction(Command c, Displayable d) {
    }

    private void deleteOffre() {
        System.out.println("delete time");
        String url = "http://localhost/MoocPHPJ2ME/deleteFormation.php";
        try {

            url += "?IdForm=" + formation_instance.getIdForm();
            //affichage requete

            System.out.println(url);

            HttpConnection hc = (HttpConnection) Connector.open(url.replace(' ', '+'));
            StringBuffer sb = new StringBuffer();
            int ch;

            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            while ((ch = dis.read()) != -1) {
                sb.append((char) ch);
            }

            System.out.println("resultat requete supression " + sb.toString());
            if ("true".equalsIgnoreCase(sb.toString().trim())) {
                edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("result", "Formation supprimé avec succes", null, AlertType.INFO), new ListFormation());
            } else {
                edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("result", "Echec de la suppression de formation", null, AlertType.ERROR), new ListFormation());
            }
            edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("result", "Formation supprimé avec succes", null, AlertType.INFO), new ListFormation());

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public void run() { 
        deleteOffre();
    }

    public void itemStateChanged(Item item) {
    }

}
