/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import mooc.ENTITIES.User;
import mooc.HANDLER.UserHandler;
import mooc.MIDLET.LoginMIDLET;
import org.xml.sax.SAXException;

/**
 *
 * @author nader
 */
public class ListeOrganismeValide extends List implements CommandListener, Runnable {

    Command cmdRefresh = new Command("refresh", Command.SCREEN, 0);
    Command cmdconsulter = new Command("Consulter", Command.SCREEN, 1);
    Command cmdretour = new Command("Retour", Command.SCREEN, 0);
    User[] people;
    StringBuffer sb;
    Display disp;

    public ListeOrganismeValide(String title, int listType) {
        super(title, listType);
        Thread th = new Thread(this);
        th.start();
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdRefresh) {
            //disp.setCurrent(loadingDialog);
            Thread th = new Thread(this);
            th.start();
        } else if (c == cmdconsulter) {
            String indexs = (this.getString(this.getSelectedIndex())).substring(0, (this.getString(this.getSelectedIndex())).indexOf(":"));
            int index = Integer.parseInt(indexs);

            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ConsulterOrganisme("Organisme", people[index]));
        } else if (c == cmdretour) {
            if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_APPRENANT\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuApprenant());
            } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_ORGANISME\";}")) {

                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuValiderOrganisme());
            } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuFormateur());
            }else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_ADMIN\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuAdmin());
            }
        }
    
    else  if (c == cmdretour && d== this) {
        
         if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_APPRENANT\";}")) {
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuApprenant());
        } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_ORGANISME\";}")) {
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuOrganisme());
        } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}")) {
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuFormateur());
        }

    }
    }

    public void run() {
        this.deleteAll();

        try {
            this.addCommand(cmdRefresh);
            this.addCommand(cmdconsulter);
            this.addCommand(cmdretour);
            this.setCommandListener(this);
            UserHandler peopleHandler = new UserHandler();
            // get a parser object

            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();

            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/SelectLogin.php?");//people.xml est un exemple

            DataInputStream dis = new DataInputStream(hc.openDataInputStream());

            parser.parse(dis, peopleHandler);

            // display the result
            people = peopleHandler.getPeople();

            if (people.length > 0) {
                for (int i = 0; i < people.length; i++) {
                    if (people[i].getRole().equals("a:1:{i:0;s:14:\"ROLE_ORGANISME\";}")) {

                        if (people[i].getEtat().equals("valide")) {
                            System.out.println(people[i].getEtat());
                            append(i + ":" + people[i].getNomsociete(), null);
                        }
                    }
                }

            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
