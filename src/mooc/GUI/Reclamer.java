/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Date;
import javax.microedition.io.CommConnection;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import mooc.ENTITIES.Reclamation;
import mooc.MIDLET.LoginMIDLET;

/**
 *
 * @author HP
 */
public class Reclamer extends Form implements CommandListener, Runnable {

    String[] choix = {"Probleme_technique", "Formation", "Contenu_indésirable"};
    ChoiceGroup tfcategorie = new ChoiceGroup("Categorie:", Choice.POPUP, choix, null);
    TextField tfemail = new TextField("Email :", "", 10000, TextField.EMAILADDR);
    //DateField Df= new DateField("Date",DateField.DATE ); 
    TextField tftext = new TextField("Message :", "", 10, TextField.ANY);
    TextField tftel = new TextField("N Telephone :", "", 10000, TextField.PHONENUMBER);
    Command cmdNext = new Command("Reclamer", Command.OK, 0);
    Command cmdBack = new Command("Retour", Command.BACK, 1);

    HttpConnection hc;
    DataInputStream dis;
    StringBuffer sb;

    String url = "http://localhost/parsing/insert.php?";

    public Reclamer(String title) {
        super("Reclamer");
        append(tfcategorie);
        append(tfemail);
        tftext.setMaxSize(5000);
        append(tftext);
        append(tftel);
        addCommand(cmdNext);
        addCommand(cmdBack);
        setCommandListener(this);

    }

    public void commandAction(Command c, Displayable d) {

        if (c == cmdNext) {

            Thread th = new Thread(this);
            th.start();
        } else if (c == cmdBack) {
            if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_APPRENANT\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuApprenant());
            } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_ORGANISME\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuOrganisme());
            } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuFormateur());
            }

        }
    }

    public void run() {
        try {

            Reclamation rec = new Reclamation();
            rec.setCategorie(tfcategorie.getString(tfcategorie.getSelectedIndex()));
            rec.setText(tftext.getString());
            rec.setEmail(tfemail.getString());
            rec.setTel(Integer.parseInt(tftel.getString()));

            hc = (HttpConnection) Connector.open("http://localhost/parsing/insert.php?categorie=" + rec.getCategorie() + "&email=" + rec.getEmail() + "&text=" + rec.getText() + "&date=" + rec.getDate() + "&etat=" + rec.getEtat() + "&iduser=" + 5 + "&tel=" + rec.getTel());
            dis = hc.openDataInputStream();

            int ascii;
            sb = new StringBuffer();

            while ((ascii = dis.read()) != -1) {

                sb.append((char) ascii);

            }

            if (sb.toString().equals("successfully added")) {
                Alert a = new Alert("Information", "Reclamation envoyer", null, AlertType.ERROR);
                a.setTimeout(3000);
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(a);

                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new Accueil());

            } else {
           //Alert a= new Alert("Information", sb.toString(), null, AlertType.ERROR);
                //a.setTimeout(3000);

            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
