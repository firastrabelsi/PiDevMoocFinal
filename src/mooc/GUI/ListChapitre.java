/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import  mooc.ENTITIES.Chapitre;
import mooc.HANDLER.ChapitreHandler;
import java.io.DataInputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 *
 * @author Dell
 */
public class ListChapitre extends List implements CommandListener, Runnable {

    String[] listData;
    Chapitre[] ChapitreData;
    TextField CritereTF = new TextField("Chapitre:", null, 1000, TextField.ANY);
   
    Command backCmd = new Command("Retour", Command.EXIT, 0);
   // Command ajouterCmd = new Command("Ajouter", Command.SCREEN, 4); 
    Command deleteCmd = new Command("Supprimer", Command.SCREEN, 4); 
    Command rechercherCmd = new Command("Rechercher", Command.SCREEN, 3); 

    Thread t;
    
    int id;
    String Nom="";
    boolean modifyFlag=false;
    
    public ListChapitre(){
        super("Chapitre", IMPLICIT);
        
        addCommand(backCmd);
        addCommand(deleteCmd);
 //       addCommand(ajouterCmd);
        addCommand(rechercherCmd);
        
        setCommandListener(this);
        t = new Thread(this);
        t.start();
    } 
    public ListChapitre(int id){
        super("Chapitre", IMPLICIT);
        this.id=id;
        modifyFlag=true;
        addCommand(backCmd);
//        addCommand(AddCmd);
//        addCommand(ajouterCmd);
        addCommand(deleteCmd);
        addCommand(rechercherCmd);

        setCommandListener(this);
        t = new Thread(this);
        t.start();
    }
    public ListChapitre(String critere){
        super("Chapitre", IMPLICIT);
        this.Nom=critere;
        addCommand(backCmd);
//        addCommand(AddCmd);
        addCommand(deleteCmd);
    //     addCommand(ajouterCmd);
        addCommand(rechercherCmd);
        
        setCommandListener(this);
        t = new Thread(this);
        t.start();
    }
    
    private Chapitre[] setAllChapitres(){
        Chapitre[] offre_table = null;
        String requete="http://localhost/MoocPHPJ2ME/selectChapitre.php";
         if(! Nom.equals(""))
        {   System.out.println("ListChapitre : CRITERE_VIDE() " +! Nom.equals(""));
            requete="http://localhost/MoocPHPJ2ME/selectChapitreParNom.php?Nom="+Nom;
            System.out.println("ListChapitre: requete affichage Chapitre par Nom: "+requete);
        } 
       
       
        
        try {
            ChapitreHandler offreHandler = new ChapitreHandler();
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            HttpConnection hc = (HttpConnection) Connector.open(requete);
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            parser.parse(dis, offreHandler);
            // display the result
             offre_table = offreHandler.getChapitre();
        } catch (Exception e) {
            System.out.println("Exception:" + e.toString());
        }
        return offre_table;
    }
       
    private void setListData()
    {
        ChapitreData = setAllChapitres();
        
        if (ChapitreData!=null)
        {
           if (ChapitreData.length!=0)
           {
            listData = new String[ChapitreData.length];
            
            for (int i = 0; i < ChapitreData.length; i++) 
            {
                listData[i] = 
                        "\n Id    : "+ChapitreData[i].getId()
                      + "\n Titre : "+ChapitreData[i].getNom()
                        + "\n Description : "+ChapitreData[i].getDescription()
                      + "\n Cours  : "+ChapitreData[i].getCour().getTitre();
            }
           }
        }
        else
        {
            append("Pas de chapitre", null);
        }
    
    }
    

    public void run() { 
        setListData();
        if (listData != null)
        {
            for (int i = 0; i < listData.length; i++) {
                append(listData[i], null);
//              append(critere, null);
            }
        }
        closeThread();
    }
    
    private void closeThread()
    {
        t.interrupt();
    }
    public void commandAction(Command c, Displayable d) { 
        if (c==deleteCmd)
        {
            edu.esprit.utils.StaticMidlet.disp.setCurrent(new DeleteChapitre(ChapitreData[this.getSelectedIndex()]));
            System.out.println(ChapitreData[this.getSelectedIndex()]);
        } 
        if (c == rechercherCmd){
            edu.esprit.utils.StaticMidlet.disp.setCurrent(new RechercheChapitre());
        }
    }
}
