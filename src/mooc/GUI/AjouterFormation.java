/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Dell
 */
public class AjouterFormation extends Form implements CommandListener, Runnable {

    private final TextField nomForm;
    private final TextField duree;
   // String[] level = {"facile", "moyen", "difficile"};
   // ChoiceGroup levell = new ChoiceGroup("level : ", ChoiceGroup.POPUP, level, null);
private final TextField level;
    

    private final Command retour;
    private final Command Ajouter;
    private HttpConnection http;
    private DataInputStream dataStream;
    private int ch;
    private final StringBuffer str = new StringBuffer("");

    public AjouterFormation() {
        super("Ajouter Formation");
        nomForm = new TextField("nomFomation", null, 30, TextField.ANY);
        duree = new TextField("la durée", null, 30, TextField.ANY);
        level = new TextField("le level", null, 30, TextField.ANY);
      /* String cat ;
        if(levell.getSelectedIndex()==0){
        cat = "facile" ;
        }
        else if(levell.getSelectedIndex()==1){
        cat = "moyen" ;
        }
        else{
        cat = "difficile" ;
        }*/



        retour = new Command("retour", Command.EXIT, 1);
        Ajouter = new Command("Ajouter", Command.SCREEN, 0);
        addCommand(retour);
        addCommand(Ajouter);

        append(nomForm); 
        append(duree);
        append(level);

        this.setCommandListener(this);

    }

    public void commandAction(Command c, Displayable d) {
        if (c == retour) {
            edu.esprit.utils.StaticMidlet.disp.setCurrent(new ListFormation());
        }
        if (c == Ajouter) {
            Thread t = new Thread(this);
            t.start();
        }
    }

    public void run() {  
        
        str.delete(0, str.length());
        boolean erreur = false;
        String erreurMsg = "";
         
       
        
        if (nomForm.getString().equals(""))
            {
                erreur = true;
                erreurMsg += "Il faut remplir votre nom.\n";
            }
        
       if (duree.getString().equals(""))
            {
                erreur = true;
                erreurMsg += "Il faut remplir la durée durée.\n";
            }
       if (level.getString().equals(""))
            {
                erreur = true;
                erreurMsg += "Il faut remplir le level.\n";
            }
        
        if (erreur)
            {
                edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("erreur", erreurMsg, null, AlertType.ERROR));
            }
        else {
            try {

                String requete="http://localhost/MoocPHPJ2ME/insertFormation.php?"
                    + "&nomForm="+nomForm.getString() + "&duree="+duree.getString() +"&level="+level.getString();
                    
                
                http = (HttpConnection) Connector.open(requete);
                System.out.println(requete);
                dataStream = http.openDataInputStream();

                while ((ch = dataStream.read()) != -1)
                {
                    str.append((char)ch);
                }
                
                System.out.println("Resultat de l'ajout de la formation --> "+str.toString());
                
                if (str.toString().equals("true"))
                {
                     edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("succes","Ajout de pays effectuer avec succée",null, AlertType.INFO),new ListFormation());
                }
                else if (str.toString().equals("false"))
                {
                     edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("erreur","Ajout de pays n'a pas été effectué",null, AlertType.ERROR),new ListFormation());
                }
                edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("succes","Ajout de pays effectuer avec succée",null, AlertType.INFO),new ListFormation());
             }
            catch (IOException ex) {
            ex.printStackTrace();
            }
        }
    }
        

    }


