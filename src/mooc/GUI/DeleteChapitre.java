/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import mooc.ENTITIES.Chapitre;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.ItemStateListener;

/**
 *
 * @author Dell
 */
public class DeleteChapitre extends Form implements CommandListener,Runnable, ItemStateListener {
      
    Chapitre chapitre_instance;
    HttpConnection http;
    DataInputStream dataStream;
    
    Thread t;

    public DeleteChapitre(Chapitre chapitre_instance){
        super("Supprimer chapitre");
        this.chapitre_instance = chapitre_instance;
        
        t = new Thread(this);
        t.start();
        this.setItemStateListener(this);
        this.setCommandListener(this);
        
    }
  private void deleteChapitre(){
        System.out.println("delete time");
    String url = "http://localhost/MoocPHPJ2ME/deleteChapitre.php";
        try {
            
            url += "?idChap="+chapitre_instance.getId();
            //affichage requete
            
            System.out.println(url);
            
            HttpConnection hc = (HttpConnection) Connector.open(url.replace(' ', '+'));
            StringBuffer sb = new StringBuffer();
            int ch;
            
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
                while ((ch = dis.read()) != -1) {                    
                    sb.append((char)ch);
                }
                
                System.out.println("resultat requete supression "+sb.toString());
                if ("true".equalsIgnoreCase(sb.toString().trim())) 
                {
                     edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("result", "chapitre supprimé avec succes", null, AlertType.INFO),new ListChapitre());
                }
                else
                {
                    edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("result", "Echec de la suppression de chapitre", null, AlertType.ERROR),new ListChapitre());
                }
                 edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("result", "ville supprimé avec succes", null, AlertType.INFO),new ListChapitre());

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    
    }
    public void commandAction(Command c, Displayable d) {
    }

    public void run() { 
        
            deleteChapitre();
    }

    public void itemStateChanged(Item item) {
    }
}
