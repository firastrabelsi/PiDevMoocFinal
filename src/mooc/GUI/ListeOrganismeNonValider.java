/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import mooc.ENTITIES.User;
import mooc.HANDLER.UserHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author nader
 */
public class ListeOrganismeNonValider extends List implements CommandListener, Runnable {
    
    Command cmdRefresh = new Command("Refresh", Command.SCREEN, 0);
    Command cmdconsulter = new Command("Consulter", Command.SCREEN, 1);
    Command cmdretour = new Command("Retour", Command.SCREEN, 0);
    User[] people;
    StringBuffer sb;
    Display disp;
    
    public ListeOrganismeNonValider(String title, int listType) {
        super(title, listType);
        Thread th = new Thread(this);
        th.start();
    }
    
    public void commandAction(Command c, Displayable d) {
        if (c == cmdRefresh) {
            //disp.setCurrent(loadingDialog);
            Thread th = new Thread(this);
            th.start();
        }
        else if (c==cmdconsulter)
        {
            String indexs=(this.getString(this.getSelectedIndex())).substring(0, (this.getString(this.getSelectedIndex())).indexOf(":"));
            int index =Integer.parseInt(indexs);

            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ConsulterOrganisme("Organisme",people[index]) );
        }
        else if (c==cmdretour)
        {
            String indexs=(this.getString(this.getSelectedIndex())).substring(0, 1);
            int index = Integer.parseInt(indexs)-1;
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuValiderOrganisme());
        }
    }
    
    public void run() {
        this.deleteAll();
        try {
            this.addCommand(cmdRefresh);
            this.addCommand(cmdconsulter);
            this.addCommand(cmdretour);
                        this.setCommandListener(this);

            UserHandler peopleHandler = new UserHandler();
            // get a parser object

            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();

            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/SelectLogin.php?");//people.xml est un exemple

            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            
            parser.parse(dis, peopleHandler);

            // display the result
            people = peopleHandler.getPeople();
            
            if (people.length > 0) {
                for (int i = 0; i < people.length; i++) {                    
                    if (people[i].getRole().equals("a:1:{i:0;s:14:\"ROLE_ORGANISME\";}")) {
                        
                        if (people[i].getEtat().equals("non valide")) {
                            
                                this.append(i+":"+people[i].getNomsociete()+"::"+people[i].getEtat(), null);
                            }
                        }                        
                    }
                }
                
                       
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }
    
}
