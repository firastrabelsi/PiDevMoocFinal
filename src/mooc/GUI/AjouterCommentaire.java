/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mooc.GUI;

/**
 *
 * @author Bali Majdi
 */
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Calendar;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextBox;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Amine Kriaa
 */
public class AjouterCommentaire extends TextBox implements CommandListener, Runnable {
   
    HttpConnection ht;
    DataInputStream dt;
    int ch;
    Display disp;
    
 Image img;
    Command cmok = new Command("OK", Command.SCREEN, 0);
    Command retour = new Command("Retour", Command.BACK, 0);
    

    public AjouterCommentaire(String title,int tx,int x,Display disp) {
        super(title, "", x, tx);
        
        
        this.disp = disp;
        disp.setCurrent(this);

        addCommand(cmok);
        addCommand(retour);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if(c==cmok){
            Thread th = new Thread(this);
            th.start();
          disp.setCurrent(new ListeCommentaire("Commentaires", List.IMPLICIT, disp));
        }
        if(c==retour){
            disp.setCurrent(new ListeCommentaire("Commentaires", List.IMPLICIT, disp));
        }
    }

    public void run() {
        
        Calendar cal = Calendar.getInstance();
        String date = cal.get(Calendar.YEAR) + "-" + ( cal.get(Calendar.MONTH) + 1 ) + "-" + cal.get(Calendar.DAY_OF_MONTH);
        String contenu = this.getString();
       // String contenu = disp.getCurrent().toString();
        int coursId=2;
        int personneid=14;
        StringBuffer str = new StringBuffer("");
        try {
            ht = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/insert.php?personne_id="+personneid+"&cours_id="+coursId+"&contenu="+contenu+"&date="+date);
            dt = ht.openDataInputStream();
            while ((ch = dt.read()) != -1) {
                str.append((char) ch);

            }

            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
                                                         try {
            img = Image.createImage("/mooc/IMAGE/add.png");
                    } catch (IOException ex) {
            ex.printStackTrace();
        }
                                                 Alert alt = new Alert("Message Ajouter !", null, img, AlertType.CONFIRMATION);
          
            disp.setCurrent(alt,new ListeCommentaire("Commentaires", List.IMPLICIT, disp));
    }

}