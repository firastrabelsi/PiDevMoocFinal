/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

//import edu.esprit.Cours.ListCours;
import  mooc.ENTITIES.Chapitre;
import  mooc.ENTITIES.Cours;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Dell
 */
public class AjouterChapitre extends Form implements CommandListener, Runnable {

    private final TextField nom;
    private final TextField description;

    private int id_cours;
    private final Command retour;
    private final Command Ajouter;
    private HttpConnection http;
   public DataInputStream dataStream;
    private int ch;
    private final StringBuffer str = new StringBuffer("");

    public AjouterChapitre(Cours cours) {
        super("Ajouter Chapitre");
        nom = new TextField("nom", null, 30, TextField.ANY);
        description = new TextField("description", null, 30, TextField.ANY);
       // id_cours = chapitre.getCour().getId();
        id_cours = cours.getId();

        retour = new Command("retour", Command.EXIT, 1);
        Ajouter = new Command("Ajouter", Command.SCREEN, 0);
        addCommand(retour);
        addCommand(Ajouter);
        append(nom);
        append(description);

        this.setCommandListener(this);
    }

    

public void commandAction(Command c, Displayable d) {
        if (c == retour) {
            edu.esprit.utils.StaticMidlet.disp.setCurrent(new ListCours());
        }
        if (c == Ajouter) {
            str.delete(0, str.length());
            boolean erreur = false;
            String erreurMsg = "";

            if (nom.getString().equals("")) {
                erreur = true;
                erreurMsg += "Il faut remplir ce champ.\n";
            }
            if (description.getString().equals("")) {
                erreur = true;
                erreurMsg += "Il faut remplir ce champ.\n";
            }
            if (erreur) {
                edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("erreur", erreurMsg, null, AlertType.ERROR));
            } else {
                try {
                    String requete="http://localhost/MoocPHPJ2ME/insertChapitre.php?"
                    + "&Nom="+nom.getString()
                    + "&Description="+description.getString()
                    + "&id_cours="+id_cours;
                
                http = (HttpConnection) Connector.open(requete);
                System.out.println(requete);
                dataStream = http.openDataInputStream();

                while ((ch = dataStream.read()) != -1)
                {
                    str.append((char)ch);
                }
                
                System.out.println("Resultat de l'ajout de chapitre --> "+str.toString());
                
                if (str.toString().equals("true"))
                {
                    edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("succes","Ajout de chapitre effectué avec succée",null, AlertType.INFO),new ListChapitre());
                }
                else if (str.toString().equals("false"))
                {
                    edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("erreur","Ajout de chapitre n'a pas été effectué",null, AlertType.ERROR),new ListChapitre());
                }
                 edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("succes","Ajout de chapitre effectué avec succée",null, AlertType.INFO),new ListChapitre());

             }
            catch (IOException ex) {
            ex.printStackTrace();
            }
        }
        }
                }
                

    
    public void run() {
    }

}
