/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import mooc.ENTITIES.User;
import mooc.MIDLET.LoginMIDLET;

/**
 *
 * @author Firas
 */
public class MenuApprenant extends Canvas implements Runnable {

    int w = getWidth();
    int h = getHeight();
    int x = 62;
    Image im;
    Image img;
    Image m;
    Image o;
    Image c;
    private Object disp;

    public MenuApprenant() {
        new Thread(this).start();

    }

    protected void paint(Graphics g) {
        g.setColor(255, 255, 255);
        try {
            img = Image.createImage("/mooc/IMAGE/BG1.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            m = Image.createImage("/mooc/IMAGE/m.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            o = Image.createImage("/mooc/IMAGE/o.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            c = Image.createImage("/mooc/IMAGE/c.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        g.drawImage(img, 0, 0, Graphics.TOP | Graphics.LEFT);
        try {
            im = Image.createImage("/mooc/IMAGE/logo_dark.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        g.setFont(Font.getFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_MEDIUM));
        g.setColor(0xbebebe);
        g.fillRect(0, x, w, 20);
        //g.setColor(255, 255, 255);
        g.drawImage(im, w / 2, 45, Graphics.BOTTOM | Graphics.HCENTER);
        g.setColor(0, 0, 0);
        g.drawImage(m, 15, 65, Graphics.TOP | Graphics.LEFT);
        g.drawString("Profil", 35, 62, Graphics.TOP | Graphics.LEFT);
        g.drawImage(o, 15, 90, Graphics.TOP | Graphics.LEFT);
        g.drawString("Cours", 35, 87, Graphics.TOP | Graphics.LEFT);
        g.drawImage(o, 15, 115, Graphics.TOP | Graphics.LEFT);
        g.drawString("Suivi cours", 35, 112, Graphics.TOP | Graphics.LEFT);
        g.drawImage(o, 15, 140, Graphics.TOP | Graphics.LEFT);
        g.drawString("Formateur", 35, 137, Graphics.TOP | Graphics.LEFT);
        g.drawImage(o, 15, 165, Graphics.TOP | Graphics.LEFT);
        g.drawString("Organisme", 35, 162, Graphics.TOP | Graphics.LEFT);
        g.drawImage(o, 15, 190, Graphics.TOP | Graphics.LEFT);
        g.drawString("Contacter nous", 35, 187, Graphics.TOP | Graphics.LEFT);
        g.drawImage(o, 15, 215, Graphics.TOP | Graphics.LEFT);
        g.drawString("Reclamation", 35, 212, Graphics.TOP | Graphics.LEFT);
        g.drawImage(o, 15, 240, Graphics.TOP | Graphics.LEFT);
        g.drawString("Formation", 35, 237, Graphics.TOP | Graphics.LEFT);
        g.drawImage(c, 15, 265, Graphics.TOP | Graphics.LEFT);

        g.drawString("Deconnexion", 35, 262, Graphics.TOP | Graphics.LEFT);
        

    }

    protected void pointerPressed(int x, int y) {
        if ((y >= 62) && (y <= 82)) {
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ModifierProfil("Profil", mooc.MIDLET.LoginMIDLET.mMidlet.display, LoginMIDLET.currentsession));
        }
        if ((y >= 87) && (y <= 107)) // disp.setCurrent(new OffrePublique("Ajouter une offre publique", disp));
        {
            if ((y >= 112) && (y <= 132)) // disp.setCurrent(new Afficher_offres("La liste des offres", disp));
            {
                if ((y >= 137) && (y <= 157)) {
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListeFormateur("Formateurs", List.IMPLICIT));
                }
            }
        }
        if ((y >= 162) && (y <= 182)) {
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListeOrganismeValide("Organismes", List.IMPLICIT));
        }
        if ((y >= 187) && (y <= 207)) //  disp.setCurrent(new ajouterReclamation("Reclamation", disp, new Menu_Clientt()));
        {
            if ((y >= 212) && (y <= 232)) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new Reclamer("Reclamation"));
            }
        }
        if ((y >= 237) && (y <= 257)) {
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListFormation());
        }
        if ((y >= 262) && (y <= 282)) {
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new Accueil());
        }

    }

    protected void keyPressed(int keyCode) {
        switch (getGameAction(keyCode)) {
            case UP:
                if (x >= 87) {
                    x = x - 25;
                }
                break;
            case DOWN:
                if (x <= 187 + 25) {
                    x = x + 25;
                }
                break;
            case FIRE:
                if ((x >= 62) && (x <= 82)) {
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ModifierProfil("Profil", mooc.MIDLET.LoginMIDLET.mMidlet.display, LoginMIDLET.currentsession));
                }
                if ((x >= 87) && (x <= 107)) {
                    //disp.setCurrent(new OffrePublique("Ajouter une offre publique", disp));
                }
                if ((x >= 112) && (x <= 132)) {
                    //disp.setCurrent(new Afficher_offres("La liste des offres", disp));
                }
                if ((x >= 137) && (x <= 157)) {
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListeFormateur("Formateurs", List.IMPLICIT));
                }
                if ((x >= 162) && (x <= 182)) {
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListeOrganismeValide("Organismes", List.IMPLICIT));
                }
                if ((x >= 187) && (x <= 207)) {
                    //disp.setCurrent(new ajouterReclamation("Reclamation", disp, new Menu_Clientt()));
                }
                if ((x >= 212) && (x <= 232)) {
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new Reclamer("Reclamer"));
                }
                if ((x >= 237) && (x <= 267)) {
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListFormation());
                }
                if ((x >= 272) && (x <= 292)) {
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new Accueil());
                }
        }
        repaint();
    }

    public void run() {
        repaint();
    }

}
