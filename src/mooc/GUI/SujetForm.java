/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;
import javax.microedition.lcdui.Ticker;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import mooc.ENTITIES.Reponse;
import mooc.ENTITIES.Sujet;
import mooc.HANDLER.ReponsesHandler;
import mooc.HANDLER.SujetsHandler;
import org.w3c.dom.Comment;
import org.xml.sax.SAXException;

/**
 *
 * @author Aymen
 */
public class SujetForm implements CommandListener, Runnable {

    Display disp;
    Form sujetForm = new Form("Sujet form");
    TextField tfSujet = new TextField("Sujet", "", 10, TextField.ANY);
    
    ChoiceGroup cg=new ChoiceGroup ("Categorie",ChoiceGroup.POPUP); 
    
    TextField tftexte = new TextField("Texte", "", 255, TextField.ANY);
    
    
    
    Command validerbtn = new Command("Nouveau sujet", Command.OK, 0);
    Command listSujetsbtn = new Command("liste sujets", Command.OK, 0);
    
    Command retour =new Command("Retour", Command.BACK, 0);

    
    List lstSujets = new List("Liste Sujets", List.IMPLICIT);
    Command addSujetbtn = new Command("Ajouter sujet", Command.OK, 0);
    Command suppSujetbtn = new Command("Supprimer sujet", Command.OK, 0);

    Form reponsesForm = new Form("Répondre");
    TextField reponsetf = new TextField("Reponse", "", 255, TextField.ANY);
    Command reponsebtn =new Command("Repondre", Command.OK, 0);
    Command retourListebtn =new Command("Retour", Command.BACK, 0);
    Ticker myTicker = new Ticker("");
    
    int selectedSujetIndex;
    
    Sujet[] sujets;
    Reponse[] reponses;

    HttpConnection hc;
    DataInputStream dis;
    String url = "http://localhost/MoocPHPJ2ME/ajout.php";
    StringBuffer sb = new StringBuffer();
    int ch;
    
    HttpConnection hc2;
    DataInputStream dis2;
    StringBuffer sb2 = new StringBuffer();
    int ch2;
    
    
    public SujetForm(Display disp) {
        this.disp = disp;
        this.startApp();
    }
    
    public void startApp() {
        
        
        lstSujets.addCommand(suppSujetbtn);
        lstSujets.addCommand(addSujetbtn);
        lstSujets.setCommandListener(this);
        
        sujetForm.append(tfSujet);
        cg.append("IOS", null); 
        cg.append("ANDROID", null); 
        cg.append("WINDOWS PHONE", null); 
        sujetForm.append(cg);
        sujetForm.append(tftexte);
        sujetForm.addCommand(validerbtn);
        sujetForm.setCommandListener(this);
        sujetForm.addCommand(listSujetsbtn);
        sujetForm.setCommandListener(this);
        runSujets();
        disp.setCurrent(lstSujets);
    }
    
    public void pauseApp() {
    }
    
    public void destroyApp(boolean unconditional) {
    }
    
    
    public void run() {
        try {
                hc = (HttpConnection) Connector.open(url+"?sujett="+tfSujet.getString().trim()+"&categorie="+cg.getString(cg.getSelectedIndex()).trim()+"&texte="+tftexte.getString().trim());
                dis = new DataInputStream(hc.openDataInputStream());
                while ((ch = dis.read()) != -1) {
                    sb.append((char)ch);
                }
                if ("OK".equals(sb.toString().trim())) {
                    System.out.println("ok");
                }else{
                    System.out.println("ko");
                }
                sb = new StringBuffer();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
   }
    
    public void runSujets(){
        
        
        
        try {
            SujetsHandler cmnt = new SujetsHandler();
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            HttpConnection ht = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/select.php");
            DataInputStream dis = new DataInputStream(ht.openDataInputStream());
            parser.parse(dis, cmnt);
            sujets = cmnt.getSujet();
            System.out.println(sujets.length);
            for(int i =0;i<sujets.length;i++){
                        
                lstSujets.append(sujets[i].getSujet()+" - "+sujets[i].getDate_pub()+" - "+sujets[i].getTexte(),null);
   
            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
       
    }
   
    
    public void runReponses(){
        
        try {
            ReponsesHandler cmnt2 = new ReponsesHandler();
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            HttpConnection ht = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/getReponses.php?id_sujet="+sujets[selectedSujetIndex].getId());
            DataInputStream dis2 = new DataInputStream(ht.openDataInputStream());
            parser.parse(dis2, cmnt2);
            reponses = cmnt2.getReponse();
            System.out.println(reponses.length);
            
            for (int i = 0; i < reponses.length; i++) {
                reponsesForm.append(reponses[i].getTexte()+" - "+reponses[i].getDate()+"\n");
            }
            
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void newReponse(){
        try {
                hc2 = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/newReponse.php?texte="+reponsetf.getString().trim()+"&id_sujet="+sujets[selectedSujetIndex].getId());
                dis2 = new DataInputStream(hc2.openDataInputStream());
                while ((ch2 = dis2.read()) != -1) {
                    sb2.append((char)ch2);
                }
                if ("OK".equals(sb2.toString().trim())) {
                    reponsesForm.deleteAll();
                    reponsesForm.append(reponsetf);
                    reponsesForm.addCommand(reponsebtn);
                    reponsesForm.setCommandListener(this);
                    reponsesForm.addCommand(retourListebtn);
                    reponsesForm.setCommandListener(this);
                    runReponses();
                    myTicker.setString("Nouveau reponse ajoutee!");
                    reponsesForm.setTicker(myTicker);
                }else{
                    myTicker.setString("");
                    reponsesForm.setTicker(myTicker);
                }
                sb = new StringBuffer();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
    }
    
    public void runsupp(){
             HttpConnection hc;
            DataInputStream dis;
            StringBuffer sb = new StringBuffer();
            int ch;
            try {
                System.out.println("_________");
                hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/suppSujet.php?id=" + sujets[selectedSujetIndex].getId());

                dis = new DataInputStream(hc.openDataInputStream());
                while ((ch = dis.read()) != -1) {
                    sb.append((char) ch);
                }
                if ("OK".equals(sb.toString().trim())) {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.CONFIRMATION);
                    a.setTimeout(3000);
                    //disp.setCurrent(a);
                    disp.setCurrent(a,new ListeSujet("Sujet", List.IMPLICIT, disp));
                } else {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    disp.setCurrent(a);
                }
                sb = new StringBuffer();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
//            Thread th=new Thread(this);
//            th.start();
    }
 
    

    public void commandAction(Command c, Displayable d) {
            
        if(c == List.SELECT_COMMAND){
            
        reponsesForm.append(reponsetf);
        reponsesForm.addCommand(reponsebtn);
        reponsesForm.setCommandListener(this);
        reponsesForm.addCommand(retourListebtn);
        reponsesForm.setCommandListener(this);
            
            selectedSujetIndex = lstSujets.getSelectedIndex();
//            System.out.println("------- "+selectedSujetIndex);
//            System.out.println("id : "+sujets[selectedSujetIndex].getId());
            runReponses();
            disp.setCurrent(reponsesForm);
        }
        
        if(c == validerbtn){
            run();
        }
        
        if(c == addSujetbtn){
            disp.setCurrent(sujetForm);
        }
        
        if(c == retour){
            disp.setCurrent(sujetForm);
        }
        
        if(c == listSujetsbtn){
            //sujetForm.deleteAll();
            runSujets();
            disp.setCurrent(lstSujets);
            
        }
        
        if(c == retourListebtn){
            reponsesForm.deleteAll();
            disp.setCurrent(lstSujets);
        }
        
        if(c == reponsebtn){
            newReponse();
        }
        
        if(c == suppSujetbtn){
            //sujetForm.deleteAll();
            runsupp();
           
            //disp.setCurrent(lstSujets);
    }
}
}