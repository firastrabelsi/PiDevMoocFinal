/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;
import javax.microedition.media.control.GUIControl;

/**
 *
 * @author Firasss
 */
public class Video extends Form implements CommandListener, Runnable {

    Display disp;
    Player player = null;
    Player player2 = null;
    Command Retour = new Command("Retour", Command.SCREEN, 0);

    public Video(String title, Display disp) {
        super(title);
        this.disp = disp;
        this.addCommand(Retour);
        this.setCommandListener(this);
        try {
            loadPlayer();
            GUIControl guiControl = (GUIControl) player.getControl("javax.microedition.media.control.GUIControl");
            if (guiControl == null) {
                throw new Exception("No GUIControl!!");
            }
            Item videoItem = (Item) guiControl.initDisplayMode(GUIControl.USE_GUI_PRIMITIVE, null);
            this.insert(0, videoItem);
            player.start();
            player2.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        disp.setCurrent(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == Retour) {
            player.close();
            player2.close();
            disp.setCurrent(new Menu(disp));

        }
    }

    public void run() {

    }

    private void loadPlayer() throws Exception {
        player = Manager.createPlayer("http://localhost/MoocPHPJ2ME/moc.GIF");
        player.realize();
        player2 = Manager.createPlayer("http://localhost/MoocPHPJ2ME/moc.wav");
        player2.realize();
    }

}
