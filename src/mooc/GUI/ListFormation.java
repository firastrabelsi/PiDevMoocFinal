/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import mooc.GUI.ListCours;
import mooc.GUI.AccueilFormateur;
import mooc.ENTITIES.Formation;
import mooc.HANDLER.FormationHandler;
import edu.esprit.utils.StaticMidlet;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author Dell
 */
public class ListFormation extends List implements CommandListener, Runnable {

    String[] listData;
    Formation[] formationData;
    TextField CritereTF = new TextField("Pays:", null, 1000, TextField.ANY);

    Command backCmd = new Command("Retour", Command.EXIT, 0);
    Command ajouterCmd = new Command("Ajouter", Command.SCREEN, 4);
    Command deleteCmd = new Command("Supprimer", Command.SCREEN, 4);
    Command ajouterVilleCmd = new Command("Ajouter Ville", Command.SCREEN, 4);
    Command rechercherCmd = new Command("Rechercher", Command.SCREEN, 3);
    Command listCours = new Command("liste des Cours", Command.SCREEN, 3);

    Thread t;

    int id;
    String nomForm = "";
    boolean modifyFlag = false;

    public ListFormation() {
        super("Formation", IMPLICIT);

        addCommand(backCmd);
        addCommand(deleteCmd);
        addCommand(ajouterCmd);
        addCommand(rechercherCmd);
        addCommand(listCours);

        setCommandListener(this);
        t = new Thread(this);
        t.start();
    }

    public ListFormation(int id) {
        super("Formation", IMPLICIT);
        this.id = id;
        modifyFlag = true;
        addCommand(backCmd);
        addCommand(deleteCmd);
        addCommand(ajouterCmd);

        addCommand(rechercherCmd);
        addCommand(listCours);

        setCommandListener(this);
        t = new Thread(this);
        t.start();
    }

    public ListFormation(String critere) {
        super("Formation", IMPLICIT);
        this.nomForm = critere;
        addCommand(backCmd);
        addCommand(deleteCmd);
        addCommand(ajouterCmd);
        addCommand(rechercherCmd);
        addCommand(listCours);

        setCommandListener(this);
        t = new Thread(this);
        t.start();
    }

    private Formation[] setAllOffres() {
        Formation[] offre_table = null;
        String requete = "http://localhost/MoocPHPJ2ME/selectFormation.php";

        if (!nomForm.equals("")) {
            System.out.println("Formationlist : CRITERE_VIDE() " + !nomForm.equals(""));
            requete = "http://localhost/MoocPHPJ2ME/selectFormationParNom.php?nomForm=" + nomForm;
            System.out.println("ListFormation: requete affichage formation par nomForm: " + requete);
        }

        try {
            FormationHandler offreHandler = new FormationHandler();
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            HttpConnection hc = (HttpConnection) Connector.open(requete);
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            parser.parse(dis, offreHandler);
            // display the result
            offre_table = offreHandler.getFormation();
        } catch (Exception e) {
            System.out.println("Exception:" + e.toString());
        }
        return offre_table;
    }

    private void setListData() {
        formationData = setAllOffres();

        if (formationData != null) {
            if (formationData.length != 0) {
                listData = new String[formationData.length];

                for (int i = 0; i < formationData.length; i++) {
                  
                 listData[i] =
                        "\n nom    : "+formationData[i].getNomForm()
                       +"\n Durée : "+formationData[i].getDuree()
                  +"\n level : "+formationData[i].getLevel(); 
                }
            }
        } else {
            append("Pas de formation", null);
        }
    }
    Image nImage;
    Alert al = new Alert("ajuter une formations");

    public void commandAction(Command c, Displayable d) {
        if (c == deleteCmd) {
            edu.esprit.utils.StaticMidlet.disp.setCurrent(new DeleteFormation(formationData[this.getSelectedIndex()]));
            System.out.println(formationData[this.getSelectedIndex()]);
        }
        if (c == backCmd) {
            edu.esprit.utils.StaticMidlet.disp.setCurrent(new MenuFormateur());
            System.out.println(formationData[this.getSelectedIndex()]);

        }

        if (c == ajouterCmd) { 
             try {
                    nImage = Image.createImage("/mooc_mobile/aa.png");

                } catch (IOException e) {
                    e.printStackTrace();
                }
                al.setImage(nImage);
                al.setTimeout(5000);
            edu.esprit.utils.StaticMidlet.disp.setCurrent(al,new AjouterFormation());
            System.out.println(formationData[this.getSelectedIndex()]);

        }
        if (c == listCours) {
            edu.esprit.utils.StaticMidlet.disp.setCurrent(new ListCours());
            System.out.println(formationData[this.getSelectedIndex()]);

        }

        if (c == rechercherCmd) {
            edu.esprit.utils.StaticMidlet.disp.setCurrent(new RechercherFormation());
        }
    }

    public void run() {
        setListData();
        if (listData != null) {
            for (int i = 0; i < listData.length; i++) {
                append(listData[i], null);
//              append(critere, null);
            }
        }
        closeThread();
    }

    private void closeThread() {
        t.interrupt();
    }

}
