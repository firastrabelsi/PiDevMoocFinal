/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Dell
 */
public class RechercheChapitre extends Form implements CommandListener {

    HttpConnection http;
    DataInputStream dataStream;
//    form elements
    TextField CritereTF = new TextField("Nom:", null, 1000, TextField.ANY);

    public RechercheChapitre() {
        super("Rechercher Chapitre");
        append(CritereTF);
        this.setCommandListener(this);
        addCommand(rechercherCmd);
        addCommand(precedent);
    }
//    commands
    Command rechercherCmd = new Command("Rechercher", Command.SCREEN, 3);
    Command precedent = new Command("Retour", Command.SCREEN, 2);

    public void commandAction(Command c, Displayable d) { 
        if (c == rechercherCmd){
            if(validerFormulaire())
            {
             edu.esprit.utils.StaticMidlet.disp.setCurrent(new ListChapitre(CritereTF.getString()));
            }
        }
        if (c==precedent)
            {
               edu.esprit.utils.StaticMidlet.disp.setCurrent(new ListChapitre());
               
            }
    
    }

    private boolean validerFormulaire() { 
        boolean continuer = true;
        String erreur = "";
        
        if (CritereTF.getString().length() == 0){
            continuer = false;
            erreur += "Veuillez remplir ce champ \n";
        }
        
        if (!continuer){
           edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("erreur", erreur, null, AlertType.ERROR));
        }
        
        return continuer;
    }

}
