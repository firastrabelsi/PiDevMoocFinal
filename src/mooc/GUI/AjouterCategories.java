/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Anouar
 */
public class AjouterCategories extends Form implements CommandListener, Runnable {

    Display disp;
    TextField categorie = new TextField("Type Categories", "", 100, TextField.ANY);

    Command cmdajouter = new Command("Ajouter", Command.OK, 0);
    Command cmdaretourner = new Command("retour", Command.OK, 0);

    //conexion
    HttpConnection hc;
    DataInputStream dis;
    String url = "http://localhost/MoocPHPJ2ME/AjouterCategorie.php?";
    StringBuffer sb = new StringBuffer();
    int ch;

    public AjouterCategories(String title, Display disp) {
        super(title);
        this.disp = disp;
        categorie.setLayout(categorie.LAYOUT_BOTTOM | categorie.LAYOUT_CENTER);
        this.append(categorie);
        this.addCommand(cmdajouter);
                this.addCommand(cmdaretourner);

        setCommandListener(this);
        disp.setCurrent(this);

    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdajouter) {
            try {
                System.out.println("******");
                url=url+"categorie=" +categorie.getString().trim();
                 System.out.println(url);
                hc = (HttpConnection) Connector.open(url);
                        
                dis = new DataInputStream(hc.openDataInputStream());
                while ((ch = dis.read()) != -1) {
                    sb.append((char) ch);
                }
                if ("OK".equals(sb.toString().trim())) {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.CONFIRMATION);
                    a.setTimeout(3000);
                    disp.setCurrent(a);
                } else {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    disp.setCurrent(a);
                }
                sb = new StringBuffer();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
         if (c == cmdaretourner) {
          disp.setCurrent(new ListCategorie("ListCategorie",List.IMPLICIT , disp));
         }
    }

    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
