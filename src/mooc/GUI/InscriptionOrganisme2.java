/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author pc
 */
public class InscriptionOrganisme2 extends Form implements CommandListener, Runnable {

    Display disp;
    Command cmdBack = new Command("Retour", Command.BACK, 1);
    Command cmd = new Command("Envoyer", Command.ITEM, 0);
    Alert a = new Alert("Information", "Votre Inscription est valide", null, AlertType.INFO);

    TextField num = new TextField("Num de tel", "", 50, TextField.NUMERIC);
    TextField certificat = new TextField("certificat", "", 50, TextField.ANY);
    TextField site = new TextField("Site de la sociéte", "", 50, TextField.ANY);

    int numb;
    String siteb;
    String certificatb;

    public InscriptionOrganisme2(String title) {
        super(title);
        this.disp = disp;
        addCommand(cmd);
        addCommand(cmdBack);
        append(num);
        append(site);
        append(certificat);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack && d == this) {
            disp.setCurrent(new login());
        }
        if (c == cmd && d == this) {
            numb = Integer.parseInt(num.getString());
            siteb = site.getString();
            certificatb = certificat.getString();

            if (numb != 0 || !"".equals(siteb) || !"".equals(certificatb)) {

                disp.setCurrent(new ApprenantHome(disp, "hello"));
                Thread th = new Thread(this);
                th.run();
            } else {
                a.setString("Verifier les Champs");
                a.setTimeout(1000);
                disp.setCurrent(a, this);
            }

        }
    }

    public void run() {
        int ch;
        int id = mooc.MIDLET.LoginMIDLET.currentsession.getId();
        StringBuffer str = new StringBuffer("");
        String ch1 = "http://localhost/MoocPHPJ2ME/insertOrganisme2.php?num=" + numb + "&site=" + siteb + "&certificat=" + certificatb + "&etat=valide" + "&id=id";

        try {
            HttpConnection ht = (HttpConnection) Connector.open(ch1);
            DataInputStream dt = ht.openDataInputStream();
            while ((ch = dt.read()) != -1) {
                str.append((char) ch);

            }

            //mess = str.toString();
            a.setTimeout(5000);
            disp.setCurrent(a, new login());

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
