/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author pc
 */
public class InscrptionOrganisme1 extends Form implements CommandListener, Runnable {
   Display disp;
    Command cmdBack = new Command("Retour", Command.BACK, 1);
    Command cmd = new Command("Envoyer", Command.ITEM, 0);
    
    TextField NomSociete = new TextField("Nom de la societé", "", 50, TextField.ANY);
    TextField email = new TextField("E-mail", "", 50, TextField.EMAILADDR);
    TextField username = new TextField("UserName", "", 50, TextField.ANY);
    TextField password = new TextField("Mot de passe", "", 50, TextField.PASSWORD); 
    TextField adresse = new TextField("Adresse", "", 50, TextField.ANY);
    
    Alert a = new Alert("Information","Votre demande doit tout d'abord etre valider par l'admin", null, AlertType.INFO);

    String adresseb;
    String Emailb;
    String passwordb;
    String useranameb;
    String NomSocieteb;
    
    public InscrptionOrganisme1(String title, Display disp) {
        super(title);
        this.disp = disp;
        addCommand(cmd);
        addCommand(cmdBack);
        append(NomSociete);
        append(username);
        append(email);
        append(password);
        append(adresse);
        
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
  if (c == cmdBack && d == this) {
            disp.setCurrent(new login());
        }
        if (c == cmd && d == this) {
            adresseb = adresse.getString();
            NomSocieteb = NomSociete.getString();
            Emailb = email.getString();
            useranameb = username.getString();
            passwordb = password.getString();
            
            if (!"".equals(adresseb) || !"".equals(NomSocieteb) || !"".equals(Emailb) || !"".equals(useranameb) || !"".equals(passwordb)) {
                
                disp.setCurrent(a,new login());
                 Thread th = new Thread(this);
                 th.run();
            } else {
                a.setString("Verifier les Champs");
                a.setTimeout(1000);
                disp.setCurrent(a, this);
            }
    
    
        }
    }

    public void run() {
            //  String mess="votre demande doit etre confirmer par l'admin ";
        int ch;
        StringBuffer str = new StringBuffer("");
        String ch1 = "http://localhost/MoocPHPJ2ME/insertOrganisme1.php?username="+useranameb+"&adresse="+ adresseb + "&NomSociete="+NomSocieteb+"&email="+Emailb+"&pwd="+passwordb+"&role=a:1:{i:0;s:14:\"ROLE_ORGANISME\";}";

        try {
            HttpConnection ht = (HttpConnection) Connector.open(ch1);
            DataInputStream dt = ht.openDataInputStream();
            while ((ch = dt.read()) != -1) {
                str.append((char) ch);

            }

            //mess = str.toString();
            a.setTimeout(5000);
            disp.setCurrent(a, new login());

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    
    }
}
