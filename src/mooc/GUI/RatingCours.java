/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import mooc.ENTITIES.Cours;
import org.xml.sax.SAXException;

/**
 *
 * @author Bali Majdi
 */
public class RatingCours extends Canvas implements CommandListener {

    HttpConnection hcc;
    DataInputStream diss;
    StringBuffer sbb = new StringBuffer();
    int ch;
    Command retour = new Command("Retour", Command.BACK, 0);
    Image img;
    Image imgBG;

    Display disp;

    int w = getWidth();
    int h = getHeight();
    int x = w / 2;
    int y = h / 2;
    int i1 = 5;
    int note;
    Image im1;
    Image im2;
    Image im3;
    int nbreV = 1;
    int idC = 2;
    Cours[] cours;
    int nbr = 0;
    int nt = 0;

    Command cmdNoter = new Command("Noter", Command.SCREEN, 0);

    public RatingCours(Display disp) {
        this.disp = disp;
        this.addCommand(cmdNoter);
        this.addCommand(retour);
        this.setCommandListener(this);
    }

    protected void paint(Graphics g) {
                      
                    
        try {
            imgBG = Image.createImage("/mooc/IMAGE/BG1.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
 
        try {
            im1 = Image.createImage("/mooc/IMAGE/starOff.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            im2 = Image.createImage("/mooc/IMAGE/starOn.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            im3 = Image.createImage("/mooc/IMAGE/logo_mooc2.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

      //  g.drawImage(imgBG, 0, 0, Graphics.TOP | Graphics.LEFT);
        g.drawImage(im3, 0, 30, Graphics.TOP | Graphics.LEFT);
        
        for (int i = 0; i < i1; i++) {

            g.drawImage(im1, x - 50 + 25 * i, h / 2, Graphics.VCENTER | Graphics.HCENTER);
        }
        
        g.drawImage(im2, x - 50, h / 2, Graphics.VCENTER | Graphics.HCENTER);

        if (x == w / 2) {
            note = 2;
        } else if (x == w / 2 + 25) {
            note = 4;
        } else if (x == w / 2 + 50) {
            note = 6;
        } else if (x == w / 2 + 75) {
            note = 8;
        } else if (x == w / 2 + 100) {
            note = 10;
        }
        
    }

    protected void keyPressed(int keyCode) {
        switch (getGameAction(keyCode)) {
            case LEFT:
                if (x >= w / 2 + 25) {
                    x = x - 25;
                    i1 = i1 + 1;
                }
                break;
            case RIGHT:
                if (x <= w / 2 + 75) {
                    x = x + 25;
                    i1 = i1 - 1;
                }
                break;
        }
        repaint();
        
    }

    public void commandAction(Command c, Displayable d) {
        if (c == retour) {
            disp.setCurrent(new ListeCommentaire("Commentaires", List.IMPLICIT, disp));
        }
        if (c == cmdNoter) {
            try {
//                    Cours cours = new Cours();
                CoursHandler cr = new CoursHandler();
                SAXParser parser = SAXParserFactory.newInstance().newSAXParser();

                HttpConnection ht = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/getCours.php" + "?id=" + idC);
                DataInputStream dis = new DataInputStream(ht.openDataInputStream());
                parser.parse(dis, cr);
                cours = cr.getCours();

                for (int i = 0; i < cours.length; i++) {
                    nbr = cours[i].getId();
                    nt = cours[i].getId();
                }
            } catch (ParserConfigurationException ex) {
                ex.printStackTrace();
            } catch (SAXException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            int a = (((nt * nbr) + note) / (nbr + 1));
            nbr++;
            System.out.println(" nbre nbre nbre " + a);

            try {
                hcc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/RatingCours.php" + "?note=" + a + "&nbreV=" + nbr + "&id=" + idC);
                diss = new DataInputStream(hcc.openDataInputStream());
                while ((ch = diss.read()) != -1) {
                    sbb.append((char) ch);
                }
                sbb = new StringBuffer();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            try {
                img = Image.createImage("/mooc/IMAGE/ok.png");
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            Alert alert = new Alert("Merci", null, img, AlertType.CONFIRMATION);

            disp.setCurrent(alert, new ListeCommentaire("Commentaires", List.IMPLICIT, disp));
        }

    }
}
