/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextBox;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Anouar
 */
public class ModifierCours extends Form implements CommandListener, Runnable {

    Display disp;
    TextField titre = new TextField("Titre", "", 50, TextField.ANY);
    TextField duree = new TextField("Duree", "", 1000, TextField.NUMERIC);
    TextBox description = new TextBox("Description", "", 1000, TextField.ANY);
    String[] choix = {"Facile", "Moyenne", "Deficile"};
    ChoiceGroup choice = new ChoiceGroup("", Choice.POPUP, choix, null);
    String[] choix1 = {"1", "2", "3"};
    ChoiceGroup choice1 = new ChoiceGroup("Categories ", Choice.POPUP, choix1, null);

    Command cmdOk = new Command("Suivant", Command.OK, 0);
    Command cmdBk = new Command("Retour", Command.BACK, 1);
    Command cmdValider = new Command("Modifier", Command.SCREEN, 0);

    //conexion
    HttpConnection hc;
    DataInputStream dis;
    String url = "http://localhost/MoocPHPJ2ME/modifierCours.php?";
    StringBuffer sb = new StringBuffer();
    int ch;

    public ModifierCours(String title, Display disp) {
        super(title);
        this.disp = disp;
        this.append(titre);
        this.append(duree);
        choice.setLayout(choice.LAYOUT_BOTTOM | choice.LAYOUT_CENTER);
        choice1.setLayout(choice.LAYOUT_BOTTOM | choice.LAYOUT_CENTER);
        this.append(choice);
        this.append(choice1);
        this.addCommand(cmdOk);
        this.addCommand(cmdBk);
        setCommandListener(this);

        description.addCommand(cmdValider);
        description.addCommand(cmdBk);
        description.setCommandListener(this);
        disp.setCurrent(this);
        titre.setString(AfficherCourNonValiderPourFormateur.coursnonvalparfor.getTitre());
        duree.setString(String.valueOf(AfficherCourNonValiderPourFormateur.coursnonvalparfor.getDureedecours()));
        description.setString(AfficherCourNonValiderPourFormateur.coursnonvalparfor.getDescription());

    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdValider) {
            try {
                System.out.println("******");
                url = url + "id=" + AfficherCourNonValiderPourFormateur.coursnonvalparfor.getId() + "&cat=" + choice1.getString(choice1.getSelectedIndex()).trim() + "&titre=" + titre.getString().trim() + "&descrip=" + description.getString() + "&diff=" + choice.getString(choice.getSelectedIndex()).trim() + "&durre=" + duree.getString().trim();
                System.out.println(url);
                hc = (HttpConnection) Connector.open(url);

                dis = new DataInputStream(hc.openDataInputStream());
                while ((ch = dis.read()) != -1) {
                    sb.append((char) ch);
                }
                if ("OK".equals(sb.toString().trim())) {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.CONFIRMATION);
                    a.setTimeout(3000);
                    disp.setCurrent(a);
                } else {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    disp.setCurrent(a);
                }
                sb = new StringBuffer();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if ((c == cmdBk) && (d == description)) {
            disp.setCurrent(this);
        }

        if ((c == cmdOk) && (d == this)) {

            disp.setCurrent(description);
        }
        if (c == cmdBk) {
   ListeNonValiderParId lstcav = new ListeNonValiderParId("Votre liste de cours non valider", List.IMPLICIT, disp);

            disp.setCurrent(lstcav);
        }

    }

    public void run() {

    }

}
