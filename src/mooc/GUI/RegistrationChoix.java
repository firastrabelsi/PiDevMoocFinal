/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

/**
 *
 * @author Firas
 */
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

public class RegistrationChoix extends Canvas implements Runnable
{
	SlideIconsMenu menu = null;
	int x=1;
	public RegistrationChoix()
	{
		Image[] im = new Image[3]; 
		
		try
		{
			im[0] = Image.createImage("/mooc/IMAGE/11.png");
			im[1] = Image.createImage("/mooc/IMAGE/22.png");
			im[2] = Image.createImage("/mooc/IMAGE/33.png");
			
			menu = new SlideIconsMenu(
				new String[]{"Organisme", "Apprenant", "Formateur"},
				im,
				getWidth(),
				getHeight()
			);
			
			new Thread(this).start();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	protected void paint(Graphics g)
	{
		menu.paint(g);
	}
	public void keyPressed(int key)
	{
		int gameKey = getGameAction(key);
		
		if(gameKey == Canvas.RIGHT)
		{
			menu.slideItem(1);
                        x=x+1;
		}
		else if(gameKey == Canvas.LEFT)
		{
			menu.slideItem(- 1);
                        x=x-1;

		}
                else if (gameKey==Canvas.FIRE)
                {
                    if(x==1)
                    {                           
                        mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new InscrptionOrganisme1("Organisme",mooc.MIDLET.LoginMIDLET.mMidlet.display));

                    }
                    else if(x==2)
                    {                           
                        mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new InscriptionApprenant("Apprenant",mooc.MIDLET.LoginMIDLET.mMidlet.display));

                    }
                    else if(x==3)
                    {                           
                        mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new InscriptionFormateur("Formateur",mooc.MIDLET.LoginMIDLET.mMidlet.display));

                    }
                
                }
	}
	public void run()
	{
		try
		{
			while(true)
			{
				repaint();
				
				synchronized(this)
				{
					wait(100L);
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
}