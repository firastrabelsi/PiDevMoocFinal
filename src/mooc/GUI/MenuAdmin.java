/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.IOException;
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import mooc.MIDLET.LoginMIDLET;

/**
 *
 * @author Firas
 */
public class MenuAdmin extends Canvas implements Runnable {

    int w = getWidth();
    int h = getHeight();
    int x = 62;
    Image im;
    Image img;
    Image m;
    Image o;
    Image c;

    public MenuAdmin() {
        new Thread(this).start();

    }

    protected void paint(Graphics g) {
        g.setColor(255, 255, 255);
        try {
            img = Image.createImage("/mooc/IMAGE/BG3.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            m = Image.createImage("/mooc/IMAGE/m.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            o = Image.createImage("/mooc/IMAGE/o.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            c = Image.createImage("/mooc/IMAGE/c.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        g.drawImage(img, 0, 0, Graphics.TOP | Graphics.LEFT);
        try {
            im = Image.createImage("/mooc/IMAGE/logo_dark.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        g.setFont(Font.getFont(Font.FACE_SYSTEM, Font.STYLE_BOLD, Font.SIZE_MEDIUM));
        g.setColor(0xbebebe);
        g.fillRect(0, x, w, 20);
        //g.setColor(255, 255, 255);
        g.drawImage(im, w / 2, 45, Graphics.BOTTOM | Graphics.HCENTER);
        g.setColor(0, 0, 0);
        g.drawImage(m, 15, 65, Graphics.TOP | Graphics.LEFT);
        g.drawString("Organisme Valide", 35, 62, Graphics.TOP | Graphics.LEFT);
        g.drawImage(o, 15, 90, Graphics.TOP | Graphics.LEFT);
        g.drawString("Organisme Non Valide", 35, 87, Graphics.TOP | Graphics.LEFT);
        g.drawImage(c, 15, 115, Graphics.TOP | Graphics.LEFT);
        g.drawString("Gerer Reclamation", 35, 112, Graphics.TOP | Graphics.LEFT);
        g.drawImage(c, 15, 140, Graphics.TOP | Graphics.LEFT);
        g.drawString("Deconnexion", 35, 137, Graphics.TOP | Graphics.LEFT);

    }

    protected void pointerPressed(int x, int y) {
        if ((y >= 62) && (y <= 82)) {
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListeOrganismeValide("Organisme", List.IMPLICIT));
        }
        if ((y >= 87) && (y <= 107)) {
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListeOrganismeNonValider("Organisme", List.IMPLICIT));
        }
        if ((y >= 112) && (y <= 132)) {
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuValiderReclamation());
        }
        if ((y >= 137) && (y <= 157)) {
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new Accueil());
        }

    }

    protected void keyPressed(int keyCode) {
        switch (getGameAction(keyCode)) {
            case UP:
                if (x >= 87) {
                    x = x - 25;
                }
                break;
            case DOWN:
                if (x <= 87) {
                    x = x + 25;
                }
                break;
            case FIRE:
                if ((x >= 62) && (x <= 82)) {
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListeOrganismeValide("Organisme", List.IMPLICIT));
                }
                if ((x >= 87) && (x <= 107)) {
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListeOrganismeNonValider("Organisme", List.IMPLICIT));
                }
                if ((x >= 112) && (x <= 132)) {
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuValiderReclamation());
                }
                if ((x >= 137) && (x <= 157)) {
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new Accueil());

                }
                if ((x >= 162) && (x <= 182)) {
                    //disp.setCurrent(new listMessage("List Message", disp, new Menu_Clientt()));
                }

        }
        repaint();
    }

    public void run() {
        repaint();
    }

}
