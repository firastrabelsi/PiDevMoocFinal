/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.IOException;
import java.util.Vector;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import mooc.ENTITIES.News;
import mooc.HANDLER.ParseThread;

/**
 *
 * @author Bali Majdi
 */
public class RSS extends List implements  CommandListener, Runnable {
    private Display disp;
    private List myNewsList;
    private Vector news;
    private Command Retour, cmdDetails;
    Image img;
    
    public RSS(String title, int listType, Display dis){
        super(title, listType);
        news = new Vector();
        disp = dis;

        myNewsList = new List("RSS", List.IMPLICIT);
        Retour = new Command("Retour", Command.EXIT, 1);
        cmdDetails = new Command("Details", Command.SCREEN, 2);
        myNewsList.addCommand(Retour);
        myNewsList.addCommand(cmdDetails);
                Thread th = new Thread(this);
        th.start();
    }
    public void commandAction(Command c, Displayable d) {
        if(c==Retour){
            disp.setCurrent(new ListeCommentaire("Commentaires", List.IMPLICIT, disp));
        }
        
 if (c== cmdDetails || c == List.SELECT_COMMAND) {
            //get the index of the selected title
            int index = myNewsList.getSelectedIndex();
            if (index != -1) {
                //get the Vector element at that index - News object
                News newsItem = (News) news.elementAt(index);

                //display the link in an Alert
                Alert message = new Alert(newsItem.getTitle(),
                        newsItem.getLink(),
                        null,
                        null);
                message.setTimeout(Alert.FOREVER);
                disp.setCurrent(message, myNewsList);

            } else {
                return;
            }
        }
        
    
    }

    public void run() {
        
            myNewsList.setCommandListener(this);
            
        //the XML file url
        String url = "http://www.itcsolutions.eu/feed/";

        ParseThread myThread = new ParseThread(this);
        //this will start the second thread
        myThread.getXMLFeed(url);
       
        disp.setCurrent(myNewsList);
    
    }
        public void addNews(News newsItem) {
        news.addElement(newsItem);
                               try {
            img = Image.createImage("/mooc/IMAGE/rss.png");
                    } catch (IOException ex) {
            ex.printStackTrace();
        }
        myNewsList.append(newsItem.getTitle(), img);
        disp.setCurrent(myNewsList);
    }

    public void DisplayError(Exception error) {
        Alert errorAlert = new Alert("Error", error.getMessage(), null, null);
        errorAlert.setTimeout(Alert.FOREVER);
        disp.setCurrent(errorAlert, myNewsList);
    }
}
