/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import mooc.ENTITIES.Formation;
import java.io.DataInputStream;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author Dell
 */
public class RechercherFormation extends Form implements CommandListener{

      Formation formation_instance;
    HttpConnection http;
    DataInputStream dataStream;
    //    commands
    Command rechercherCmd = new Command("Rechercher", Command.SCREEN, 3);
    Command precedent = new Command("Retour", Command.EXIT, 0);

    //    form elements
    TextField CritereTF = new TextField("veuillez saisir le nom de la Formation:", null, 1000, TextField.ANY);
    public RechercherFormation(){
        super("Rechercher Formation");
        append(CritereTF);
        this.setCommandListener(this);
        addCommand(rechercherCmd);
        addCommand(precedent);
    }    
    
    
    public void commandAction(Command c, Displayable d) { 
        if (c == rechercherCmd){
            if(validerFormulaire())
            {
              edu.esprit.utils.StaticMidlet.disp.setCurrent(new ListFormation(CritereTF.getString()));
            }
        }
        if (c==precedent)
            {
                edu.esprit.utils.StaticMidlet.disp.setCurrent(new ListFormation());
            }
    }
    private boolean validerFormulaire(){
        boolean continuer = true;
        String erreur = "";
        
        if (CritereTF.getString().length() == 0){
            continuer = false;
            erreur += "veuillez remplir ce champ \n";
        }
        
        if (!continuer){
               edu.esprit.utils.StaticMidlet.disp.setCurrent(new Alert("erreur", erreur, null, AlertType.ERROR));
        }
        
        return continuer;
    }
    } 
    
    

