/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import mooc.ENTITIES.User;
import mooc.HANDLER.UserHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author Firas
 */
public class login extends Form implements CommandListener, Runnable {

    TextField tfusername = new TextField("UserName", "", 20, TextField.ANY);
    TextField tfpassword = new TextField("Mot de passe", "", 20000, TextField.PASSWORD);
    Command cmdBack = new Command("Retour", Command.BACK, 0);
    Command cmdlogin = new Command("Login", Command.OK, 2);
    HttpConnection hc;
    DataInputStream dis;
    StringBuffer sb;
   public static User[] users;
    int id;

    public login() {
        super("Login");
        tfpassword.setMaxSize(20);
        append(tfusername);
        append(tfpassword);
        this.addCommand(cmdlogin);
        this.addCommand(cmdBack);
        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdlogin) {
            Thread th = new Thread(this);
            th.start();

        } else if (c == cmdBack) {
           
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new login());

            
        }

    }

    public void run() {

        try {
            setCommandListener(this);
            addCommand(cmdlogin);
            UserHandler DataHandler = new UserHandler();
            // get a parser object
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)

            HttpConnection hc = (HttpConnection) Connector.
                    open("http://localhost/MoocPHPJ2ME/SelectLogin.php?");//people.xml est un exemple
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());

            parser.parse(dis, DataHandler);
            // display the result
            users = DataHandler.getPeople();
            int ascii;
            sb = new StringBuffer();

            while ((ascii = dis.read()) != -1) {

                sb.append((char) ascii);

            }

            if (users.length > 0) {

                for (int i = 0; i < users.length; i++) {
                    if (users[i].getUsername().equals(tfusername.getString())) {
                        if (users[i].getPassword().equals(tfpassword.getString())) {
                            id = i;
                        }
                    }
                }
                if (users[id].getUsername().equals(tfusername.getString().trim())) {
                    if (users[id].getPassword().equals(tfpassword.getString())) {
                        mooc.MIDLET.LoginMIDLET.currentsession = users[id];

                        if (users[id].getRole().equals("a:1:{i:0;s:14:\"ROLE_APPRENANT\";}")) {
                            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuApprenant());

                        } else if (users[id].getRole().equals("a:1:{i:0;s:10:\"ROLE_ADMIN\";}")) {

                            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuAdmin());

                        } else if (users[id].getRole().equals("a:1:{i:0;s:14:\"ROLE_ORGANISME\";}")) {

                            if (users[id].getEtat().equals("valide")) {
                                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuOrganisme());
                            } else if (users[id].getEtat().equals("valide a terminer")) {
                                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new InscriptionOrganisme2("organisme"));

                            }

                        } else if (users[id].getRole().equals("a:1:{i:0;s:14:\"ROLE_COMITE\";}")) {
                            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuComite());

                        } else if (users[id].getRole().equals("a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}")) {
                            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuFormateur());

                        }

                    } else {
                        Alert a = new Alert("Information", "Votre mot de passe est incorrect", null, AlertType.ERROR);
                        a.setTimeout(3000);
                        mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(a);

                    }
                } else {
                    Alert a = new Alert("Information", "Verifier vos informations", null, AlertType.ERROR);
                    a.setTimeout(3000);
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(a);

                }

            } else {
                Alert a = new Alert("Information", "yabta chwaya", null, AlertType.ERROR);
                a.setTimeout(3000);
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(a);

            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
