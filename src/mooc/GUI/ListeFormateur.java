/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import mooc.ENTITIES.User;
import mooc.HANDLER.UserHandler;
import mooc.MIDLET.LoginMIDLET;
import org.xml.sax.SAXException;

/**
 *
 * @author nader
 */
public class ListeFormateur extends List implements CommandListener, Runnable {

    Command cmdRefresh = new Command("Raffraichir", Command.SCREEN, 0);
    Command cmdBack = new Command("Retour", Command.BACK, 1);

    User[] people;
    StringBuffer sb;
    Display disp;

    public ListeFormateur(String title, int listType) {
        super(title, listType);
        Thread th = new Thread(this);
        th.start();
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdRefresh) {
            //disp.setCurrent(loadingDialog);
            Thread th = new Thread(this);
            th.start();
        }else  if (c == cmdBack && d == this) {
        
         if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_APPRENANT\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent( new MenuApprenant());
            } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_ORGANISME\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent( new MenuOrganisme());
            } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent( new MenuFormateur());
            }
        
        
        }
    }

    public void run() {
        this.deleteAll();

        try {
            this.setCommandListener(this);
            this.addCommand(cmdRefresh);
            this.addCommand(cmdBack);
            UserHandler peopleHandler = new UserHandler();
            // get a parser object

            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();

            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/SelectLogin.php?");//people.xml est un exemple

            DataInputStream dis = new DataInputStream(hc.openDataInputStream());

            parser.parse(dis, peopleHandler);

            // display the result
            people = peopleHandler.getPeople();

            if (people.length > 0) {
                for (int i = 0; i < people.length; i++) {
                    if (people[i].getRole().equals("a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}")) {

                        append(people[i].getUsername() + "::" + people[i].getSpecialite(), null);

                    }
                }

            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
