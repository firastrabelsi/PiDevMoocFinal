/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import mooc.ENTITIES.User;
import mooc.MIDLET.LoginMIDLET;

/**
 *
 * @author pc
 */
public class ModifierProfil extends Form implements CommandListener, Runnable {

    User u1 = new User();
    Display disp;
    String rol;
    Command cmdBack = new Command("Retour", Command.BACK, 0);
    Command cmd = new Command("Modifier", Command.ITEM, 1);
    TextField nom = new TextField("Nom", "", 50, TextField.ANY);
    TextField prenom = new TextField("Prenom", "", 50, TextField.ANY);
    TextField email = new TextField("E-mail", "", 50, TextField.EMAILADDR);
    TextField username = new TextField("UserName", "", 50, TextField.ANY);
    TextField password = new TextField("Mot de passe", "", 50, TextField.PASSWORD);
    TextField specialite = new TextField("specialite", "", 50, TextField.ANY);
    TextField num = new TextField("Num de tel", "", 50, TextField.NUMERIC);
    TextField certificat = new TextField("certificat", "", 50, TextField.ANY);
    TextField site = new TextField("Site de la sociéte", "", 50, TextField.ANY);
    TextField adresse = new TextField("Adresse", "", 50, TextField.ANY);
    TextField NomSociete = new TextField("Nom de la societé", "", 50, TextField.ANY);

    Alert a = new Alert("Information", "succes", null, AlertType.INFO);
    String nomb;
    String prenomb;
    String Emailb;
    String passwordb;
    String useranameb;
    String specialiteb;
    int numb;
    String siteb;
    String certificatb;
    String NomSocieteb;
    String adresseb;

    public ModifierProfil(String title, Display disp, User u) {
        super(title);
        this.disp = disp;
        nom.setString(u.getNom());
        prenom.setString(u.getPrenom());
        email.setString(u.getEmail());
        username.setString(u.getUsername());
        password.setString(u.getPassword());
        specialite.setString(u.getSpecialite());
        num.setString(Integer.toString(u.getNum()));
        certificat.setString(u.getCertificat());
        site.setString(u.getSite());
        adresse.setString(u.getAdresse());
        NomSociete.setString(u.getNomsociete());
        addCommand(cmdBack);
        addCommand(cmd);
        append(username);
        append(password);
        append(email);

        if (u.getRole().equals("a:1:{i:0;s:14:\"ROLE_APPRENANT\";}")) {
            append(nom);
            append(prenom);

        } else if (u.getRole().equals("a:1:{i:0;s:14:\"ROLE_ORGANISME\";}")) {

            append(NomSociete);
            append(adresse);
            append(site);
            append(num);
            append(certificat);

        } else if (u.getRole().equals("a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}")) {
            append(specialite);
        }

        setCommandListener(this);

    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack && d == this) {
            if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_APPRENANT\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuApprenant());
            } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_ORGANISME\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuOrganisme());
            } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuFormateur());
            }
        }
        if (c == cmd && d == this) {

            Emailb = email.getString();
            useranameb = username.getString();
            passwordb = password.getString();
            if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_APPRENANT\";}")) {
                nomb = nom.getString();
                prenomb = prenom.getString();
                adresseb = adresse.getString();
                numb = 0;
                siteb = "";
                certificatb = "";
                NomSocieteb = "";
                specialiteb = "";

            } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_ORGANISME\";}")) {
                nomb = "";
                prenomb = "";
                specialiteb = "";

                numb = Integer.parseInt(num.getString());
                siteb = site.getString();
                certificatb = certificat.getString();
                adresseb = adresse.getString();
                NomSocieteb = NomSociete.getString();
            } else if (LoginMIDLET.currentsession.getRole().equals("a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}")) {
                specialiteb = specialite.getString();
                nomb = "";
                prenomb = "";
                specialiteb = "";

                numb = 0;
                siteb = "";
                certificatb = "";
                NomSocieteb = "";
                specialiteb = "";
            }

            if (!"".equals(Emailb) || !"".equals(useranameb) || !"".equals(passwordb)) {
                //mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ModifierProfil("Modifier", disp, mooc.HANDLER.UserHandler.u));

                Thread th = new Thread(this);
                th.run();
            } else {
                a.setString("Verifier les Champs");
                a.setTimeout(2000);
                disp.setCurrent(a, this);
            }

        }
    }

    public void run() {
        mooc.MIDLET.LoginMIDLET.currentsession.setUsername(useranameb);
        mooc.MIDLET.LoginMIDLET.currentsession.setEmail(Emailb);
        mooc.MIDLET.LoginMIDLET.currentsession.setPassword(passwordb);
        mooc.MIDLET.LoginMIDLET.currentsession.setNom(nomb);
        mooc.MIDLET.LoginMIDLET.currentsession.setPrenom(prenomb);
        mooc.MIDLET.LoginMIDLET.currentsession.setAdresse(adresseb);
        mooc.MIDLET.LoginMIDLET.currentsession.setNum(numb);
        mooc.MIDLET.LoginMIDLET.currentsession.setSite(siteb);
        mooc.MIDLET.LoginMIDLET.currentsession.setCertificat(certificatb);
        mooc.MIDLET.LoginMIDLET.currentsession.setSpecialite(specialiteb);
        mooc.MIDLET.LoginMIDLET.currentsession.setNomsociete(NomSocieteb);
        int ch;
        StringBuffer str = new StringBuffer("");
        String ch1 = "http://localhost/MoocPHPJ2ME/Modifier.php?username=" + useranameb + "&nom=" + nomb + "&prenom=" + prenomb + "&email=" + Emailb + "&pwd=" + passwordb + "&specialite=" + specialiteb + "&num=" + numb + "&site=" + siteb + "&certificat=" + certificatb + "&adresse=" + adresseb + "&NomSociete=" + NomSocieteb + "&id=" + mooc.MIDLET.LoginMIDLET.currentsession.getId();
        try {
            HttpConnection ht = (HttpConnection) Connector.open(ch1);
            DataInputStream dt = ht.openDataInputStream();
            mooc.MIDLET.LoginMIDLET.currentsession.setUsername(useranameb);
            mooc.MIDLET.LoginMIDLET.currentsession.setEmail(Emailb);
            mooc.MIDLET.LoginMIDLET.currentsession.setPassword(passwordb);
            mooc.MIDLET.LoginMIDLET.currentsession.setNom(nomb);
            mooc.MIDLET.LoginMIDLET.currentsession.setPrenom(prenomb);
            mooc.MIDLET.LoginMIDLET.currentsession.setAdresse(adresseb);
            mooc.MIDLET.LoginMIDLET.currentsession.setNum(numb);
            mooc.MIDLET.LoginMIDLET.currentsession.setSite(siteb);
            mooc.MIDLET.LoginMIDLET.currentsession.setCertificat(certificatb);
            mooc.MIDLET.LoginMIDLET.currentsession.setSpecialite(specialiteb);
            mooc.MIDLET.LoginMIDLET.currentsession.setNomsociete(NomSocieteb);
            while ((ch = dt.read()) != -1) {
                str.append((char) ch);

            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
