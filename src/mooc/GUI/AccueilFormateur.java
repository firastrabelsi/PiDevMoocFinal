package mooc.GUI;

import mooc.GUI.ListChapitre;
import mooc.GUI.ListCours;
import mooc.GUI.AjouterFormation;
import mooc.GUI.ListFormation;
import java.io.IOException;
import javax.microedition.lcdui.Alert;
//import static javax.microedition.lcdui.Choice.IMPLICIT;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;

import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Dell
 */
public class AccueilFormateur extends List implements CommandListener, Runnable {

    private String[] selection = {"Formation", "Cours", "Chapitre"};
    TextField CritereTF = new TextField("Formateur:", null, 1000, TextField.ANY);

    Command backCmd = new Command("Retour", Command.EXIT, 0);

    Thread t;

    int id;
    String critere = "";
    boolean modifyFlag = false;

    public AccueilFormateur() {
        super("Formateur",IMPLICIT);
        addCommand(backCmd);

        setCommandListener(this);
        t = new Thread(this);
        t.start();
    }

    public AccueilFormateur(int id) {
        super("Formateur",IMPLICIT);
        this.id = id;
        modifyFlag = true;
        addCommand(backCmd);
        setCommandListener(this);
        t = new Thread(this);
        t.start();
    }

    public AccueilFormateur(String critere) {
        super("Formateur",IMPLICIT);
        this.critere = critere;
        addCommand(backCmd);

        setCommandListener(this);
        t = new Thread(this);
        t.start();
    }
 
    public void commandAction(Command c, Displayable d) {

        if (List.SELECT_COMMAND == c) {
            String cmd = selection[this.getSelectedIndex()];
            if (cmd.equals("Formation")) {
                edu.esprit.utils.StaticMidlet.disp.setCurrent(new ListFormation());
                System.out.println(selection[this.getSelectedIndex()]);
            }
            if (cmd.equals("Cours")) {
                edu.esprit.utils.StaticMidlet.disp.setCurrent(new ListCours());
                System.out.println(selection[this.getSelectedIndex()]);
            }
            if (cmd.equals("Chapitre")) {
                edu.esprit.utils.StaticMidlet.disp.setCurrent(new ListChapitre());
                System.out.println(selection[this.getSelectedIndex()]);
            }
        }
    }

    public void run() {
        if (selection != null) {
            for (int i = 0; i < selection.length; i++) {
                append(selection[i], null);
//              append(critere, null);
            }
        }
        closeThread();
    }

    private void closeThread() {
        t.interrupt();
    }

}
