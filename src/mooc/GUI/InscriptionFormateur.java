/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;

/**
 *
 * @author pc
 */
public class InscriptionFormateur extends Form implements CommandListener, Runnable {
    Display disp;
    Command cmdBack = new Command("Retour", Command.BACK, 0);
    Command cmd = new Command("Envoyer", Command.ITEM, 1);
    TextField nom = new TextField("Nom", "", 50, TextField.ANY);
    TextField prenom = new TextField("Prenom", "", 50, TextField.ANY);
    TextField email = new TextField("E-mail", "", 50, TextField.EMAILADDR);
    TextField username = new TextField("UserName", "", 50, TextField.ANY);
    TextField password = new TextField("Mot de passe", "", 50, TextField.PASSWORD);
    TextField specialite= new TextField("specialite", "", 50, TextField.ANY);
    
    Alert a = new Alert("Information","succes de l'inscription", null, AlertType.INFO);
 
    
    String nomb;
    String prenomb;
    String Emailb;
    String passwordb;
    String useranameb;
    String specialiteb;

    public InscriptionFormateur(String title,Display disp) {
        super(title);
        this.disp = disp;
        addCommand(cmdBack);
        addCommand(cmd);
        append(username);
        append(nom);
        append(prenom);
        append(email);
        append(password);
        append(specialite);

        setCommandListener(this);
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack && d == this) {
            disp.setCurrent(new login());
        }
        if (c == cmd && d == this) {
            nomb = nom.getString();
            prenomb = prenom.getString();
            Emailb = email.getString();
            useranameb = username.getString();
            passwordb = password.getString();
            specialiteb = specialite.getString();

            if (!"".equals(nomb) || !"".equals(prenomb) || !"".equals(Emailb) || !"".equals(useranameb) || !"".equals(passwordb) || !"".equals(specialiteb)) {
                disp.setCurrent(new ApprenantHome(disp, "hello"));
                Thread th = new Thread(this);
                th.run();
            } else {
                a.setString("Verifier les Champs");
                a.setTimeout(2000);
                disp.setCurrent(a, this);
            }

        }
    }

    public void run() {

      int ch;
        StringBuffer str = new StringBuffer("");
        String ch1 = "http://localhost/MoocPHPJ2ME/insertformateur.php?username=" + useranameb + "&nom=" + nomb + "&prenom=" + prenomb + "&email=" + Emailb + "&pwd=" + passwordb + "&role=a:1:{i:0;s:14:\"ROLE_FORMATEUR\";}"+"&specialite="+specialiteb;

        try {
            HttpConnection ht = (HttpConnection) Connector.open(ch1);
            DataInputStream dt = ht.openDataInputStream();
            while ((ch = dt.read()) != -1) {
                str.append((char) ch);

            }

            a.setTimeout(1000);
            disp.setCurrent(a, new login());

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    
    
    
    }
    
    
    }
    

