/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import mooc.ENTITIES.Cours;

/**
 *
 * @author Anouar
 */
public class AfficherCoursAValider extends Form implements CommandListener {
    static Cours courschek;

    Command cmdBack = new Command("Retour", Command.BACK, 0);
    Command cmdcheck = new Command("Valider Cours", Command.OK, 0);

    Display disp;

    public AfficherCoursAValider(String title, Display d) {
        super(title);
        addCommand(cmdBack);
        addCommand(cmdcheck);        
        setCommandListener(this);
        disp = d;
    }

    public void commandAction(Command c, Displayable d) {
   if (c == cmdBack) {
            ListCoursNonValider lstp = new ListCoursNonValider("Cours Non Valider", List.IMPLICIT, disp);
            disp.setCurrent(lstp);
        }
        if (c == cmdcheck) {
               HttpConnection hc;
            DataInputStream dis;
            StringBuffer sb = new StringBuffer();
            int ch;
            try {
                System.out.println("******");
                hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/modifierEtatCours.php?id=" +courschek.getId());

                dis = new DataInputStream(hc.openDataInputStream());
                while ((ch = dis.read()) != -1) {
                    sb.append((char) ch);
                }
                if ("OK".equals(sb.toString().trim())) {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.CONFIRMATION);
                    a.setTimeout(3000);
                    disp.setCurrent(a);
                } else {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    disp.setCurrent(a);
                }
                sb = new StringBuffer();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            
        }
    }

}
