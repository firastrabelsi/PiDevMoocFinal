/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.IOException;
import javax.microedition.lcdui.Canvas;


import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;

/**
 *
 * @author Anouar
 */
public class Menu2 extends Canvas {

    int w = getWidth();
    int h = getHeight();

    int x = 62;
    Image im;
    Image img;
    Display disp;

    public Menu2(Display disp) {

        this.disp = disp;
    }

    protected void paint(Graphics g) {
        g.setColor(255, 255, 255);
        try {
            img = Image.createImage("/mooc/IMAGE/bg.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        g.drawImage(img, 0, 0, Graphics.TOP | Graphics.LEFT);
        try {
            im = Image.createImage("/mooc/IMAGE/puce.png");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        g.setColor(0xbebebe);
        g.fillRect(0, x, w, 20);
        g.setColor(255, 255, 255);
        g.drawString("****Menu****", w / 2, 45, Graphics.BASELINE | Graphics.HCENTER);
        g.setColor(0, 0, 0);
        g.drawImage(im, 5, 65, Graphics.TOP | Graphics.LEFT);
        g.drawString("Votre liste de cours non valider", 25, 62, Graphics.TOP | Graphics.LEFT);
        g.drawImage(im, 5, 90, Graphics.TOP | Graphics.LEFT);
        g.drawString("Liste de categories", 25, 87, Graphics.TOP | Graphics.LEFT);
       

        
//        g.drawString("Video", 25, 162, Graphics.TOP | Graphics.LEFT);
//        g.drawImage(im, 5, 190, Graphics.TOP | Graphics.LEFT);
//        g.drawString("Video", 25, 187, Graphics.TOP | Graphics.LEFT);
////            g.drawImage(im, 5, 215, Graphics.TOP | Graphics.LEFT);
//            g.drawString("Deconnexion", 25, 212, Graphics.TOP | Graphics.LEFT);
    }

    protected void pointerPressed(int x, int y) {
        if ((y >= 62) && (y <= 82)) {
            ListeNonValiderParId lstcav = new ListeNonValiderParId("Votre liste de cours non valider", List.IMPLICIT, disp);

            disp.setCurrent(lstcav);
        }
        if ((y >= 87) && (y <= 107)) {
           ListCategorie lstcat = new ListCategorie("List Categories", List.IMPLICIT, disp);

            disp.setCurrent(lstcat);
        }
       
    }

    protected void keyPressed(int keyCode) {
        switch (getGameAction(keyCode)) {
            case UP:
                if (x >= 87) {
                    x = x - 25;
                }
                break;
            case DOWN:
                if (x <= 187) {
                    x = x + 25;
                }
                break;
            case FIRE:
                if ((x >= 62) && (x <= 82)) {
                    ListeNonValiderParId lstcav = new ListeNonValiderParId("Votre liste de cours non valider", List.IMPLICIT, disp);

                    disp.setCurrent(lstcav);
                }
                if ((x >= 87) && (x <= 107)) {
                      ListCategorie lstcat = new ListCategorie("List Categories", List.IMPLICIT, disp);

            disp.setCurrent(lstcat);
                }
                
                
               
        }
        repaint();
    }

}
