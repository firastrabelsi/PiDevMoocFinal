/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import mooc.ENTITIES.Cours;
import mooc.ENTITIES.SuivCours;

/**
 *
 * @author Anouar
 */
public class AfficherCoursValider extends Form implements CommandListener, Runnable {

    static Cours coursvalid;

    Command cmdBack = new Command("Retour", Command.BACK, 0);
    Command cmdins = new Command("Inscrit au cours", Command.OK, 0);
    Command cmdsupinscr = new Command("Déinscrit au cours", Command.OK, 0);

    Display disp;

    public AfficherCoursValider(String title, Display d) {
        super(title);
        addCommand(cmdBack);
        addCommand(cmdins);
        addCommand(cmdsupinscr);
        setCommandListener(this);
        disp = d;
    }
    HttpConnection hc;
    DataInputStream dis;
    StringBuffer sb = new StringBuffer();
    int ch;

    public void commandAction(Command c, Displayable d) {
        if (c == cmdBack) {
            ListouCoursValider lstp = new ListouCoursValider("Cours Valider", List.IMPLICIT, disp);
            disp.setCurrent(lstp);
        }
        if (c == cmdins) {

            if (verif() == -1) {
                Alert a = new Alert("Vous etes inscrit", sb.toString(), null, AlertType.CONFIRMATION);
                a.setTimeout(3000);
                disp.setCurrent(a);

            } else if (verif() == 1) {
                try {
                    hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/AjouterSuiv.php?cour=" + coursvalid.getId() + "&utilisa=3");

                    dis = new DataInputStream(hc.openDataInputStream());
                    while ((ch = dis.read()) != -1) {
                        sb.append((char) ch);
                    }
                    if ("OK".equals(sb.toString().trim())) {
                        Alert a = new Alert("votre suivi bien sauvegarder", sb.toString(), null, AlertType.CONFIRMATION);
                        a.setTimeout(3000);
                        disp.setCurrent(a);
                    } else {
                        Alert a = new Alert("Erreur", sb.toString(), null, AlertType.ERROR);
                        a.setTimeout(3000);
                        disp.setCurrent(a);
                    }
                    sb = new StringBuffer();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }

        }
        if (c == cmdsupinscr) {
            if (verif() == -1) {

                try {
                    hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/supprimerSuivcours.php?idcour=" + coursvalid.getId() +"&idutil=3");

                    dis = new DataInputStream(hc.openDataInputStream());
                    while ((ch = dis.read()) != -1) {
                        sb.append((char) ch);
                    }
                    if (!("OK".equals(sb.toString().trim()))) {
                        Alert a = new Alert("votre désinscrire bien sauvegarder", sb.toString(), null, AlertType.CONFIRMATION);
                        a.setTimeout(3000);
                        disp.setCurrent(a);
                    } else {
                        Alert a = new Alert("Erreur", sb.toString(), null, AlertType.ERROR);
                        a.setTimeout(3000);
                        disp.setCurrent(a);
                    }
                    sb = new StringBuffer();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            } else if (verif() == 1) {
                Alert a = new Alert("Vous n' etes pas inscrit", sb.toString(), null, AlertType.CONFIRMATION);
                a.setTimeout(3000);
                disp.setCurrent(a);

            }

        }
    }

    public void run() {

    }

    public int verif() {

        try {
            System.out.println("******");
String url ="http://localhost/MoocPHPJ2ME/verficationUtisateurSuiv.php?utilisateur=3&cours=" +coursvalid.getId();
            System.out.println(url);
            hc = (HttpConnection) Connector.open(url);
            System.out.println(coursvalid.getId());

            dis = new DataInputStream(hc.openDataInputStream());
            while ((ch = dis.read()) != -1) {
                sb.append((char) ch);
            }
            if (!("0".equals(sb.toString().trim()))) {
               return  -1;
               
            } 
            sb = new StringBuffer();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
return 1;
    }

}
