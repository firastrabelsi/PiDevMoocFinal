/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import mooc.ENTITIES.Commentaire;
import mooc.HANDLER.CommentaireHandler;
import mooc.MIDLET.LoginMIDLET;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import mooc.ENTITIES.User;
import mooc.HANDLER.UserHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author Bali Majdi
 */
public class ListeCommentaire extends List implements Runnable, CommandListener {

    Image img;
    Commentaire[] commentaires;
    User[] user;
    User[] user2;
    Display disp;
    StringBuffer sb = new StringBuffer();
    Display disp2;
    StringBuffer sb2 = new StringBuffer();
    Command cmretour;
    Command cmd = new Command("Commenter", Command.OK, 0);
    Command vote = new Command("Noter Cours", Command.OK, 0);
    Command supp = new Command("Supprimer", Command.OK, 0);
    Command RSS = new Command("Flux RSS", Command.OK, 0);
    List list;
    HttpConnection hc;
    DataInputStream dis;
    HttpConnection hc2;
    DataInputStream dis2;
    String url = "http://localhost/MoocPHPJ2ME/supprimerCommentaire.php?id=";

    int ch;

    public ListeCommentaire(String title, int listType, Display d) {
        super(title, listType);

        disp = d;
        cmretour = new Command("Retour", Command.SCREEN, 0);
        this.addCommand(cmd);
        this.addCommand(vote);
        this.addCommand(supp);
        this.addCommand(RSS);
        Thread th = new Thread(this);
        th.start();
    }

    public void run() {
        this.deleteAll();
        setCommandListener(this);
        try {
            CommentaireHandler cmnt = new CommentaireHandler();
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            HttpConnection ht = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/getComment.php");
            DataInputStream dis = new DataInputStream(ht.openDataInputStream());
            parser.parse(dis, cmnt);
            commentaires = cmnt.getCommentaire();
            try {
                img = Image.createImage("/mooc/IMAGE/user.png");
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            for (int i = 0; i < commentaires.length; i++) {
                System.out.println(" test test " + commentaires[i].toString());

                int idut = commentaires[i].getUt();
                System.out.println("id utilisateur test test **** " + idut);
                try {
                    UserHandler users = new UserHandler();
                    SAXParser parser2 = SAXParserFactory.newInstance().newSAXParser();
                    HttpConnection ht2 = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/SelectLogin.php");
                    DataInputStream dis2 = new DataInputStream(ht2.openDataInputStream());
                    parser2.parse(dis2, users);
                    user = users.getPeople();
                    
                } catch (ParserConfigurationException ex) {
                    ex.printStackTrace();
                } catch (SAXException ex) {
                    ex.printStackTrace();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                String ussName="";
       for (int j = 0; j < user.length; j++) {
       if (commentaires[i].getUt()==user[j].getId()){
       ussName = user[j].getNom();
       }
       }
       
                System.out.println(" test2 test2 " + user[i].getNom());

                append("\n" + ussName +  "\n" + commentaires[i].toString() + "\n", img);

                System.out.println("22222" + commentaires[i].toString());

            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public void commandAction(Command c, Displayable d) {
        if (c == supp) {
            Commentaire a = commentaires[getSelectedIndex()];

            try {
                hc = (HttpConnection) Connector.open(url + a.getId());
                dis = new DataInputStream(hc.openDataInputStream());
                while ((ch = dis.read()) != -1) {
                    sb.append((char) ch);

//            this.run();
                }

                sb = new StringBuffer();
            } catch (IOException ex) {
            }

//                                           Thread th = new Thread(this);
//            th.start();
            try {
                img = Image.createImage("/mooc/IMAGE/trash.png");
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            Alert alert = new Alert("Message supprimer", null, img, AlertType.CONFIRMATION);

            disp.setCurrent(alert, new ListeCommentaire("Commentaires", List.IMPLICIT, disp));

        }
        if (c == cmd) {
            disp.setCurrent(new AjouterCommentaire("Commenter", TextField.ANY, 250, disp));
        }

        if (c == vote) {
            disp.setCurrent(new RatingCours(disp));
        }
        if (c == RSS) {
            disp.setCurrent(new RSS("RSS", List.IMPLICIT, disp));
        }
    }
}
