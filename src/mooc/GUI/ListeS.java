/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mooc.GUI;
import mooc.MIDLET.LoginMIDLET;
import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.StringItem;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import mooc.ENTITIES.Sujet;
import mooc.HANDLER.SujetsHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author Aymen
 */
public class ListeS extends List implements Runnable,CommandListener{
    
    
      Display disp;
    Command cmretour ;
    List list ;



    public ListeS(String title, int listType , Display d) {
        super(title, listType);
        list = this;
        
        disp=d;
        
        
        append("Forum", null);
         append("googlemap", null);
        
        cmretour = new Command("Retour", Command.SCREEN , 0);
        
        
        addCommand(cmretour);
        Thread th = new Thread(this);
       setCommandListener(this);
    }

    public void run() {
        
        
    }

    public void commandAction(Command c, Displayable d) {

    if(list.getSelectedIndex()==0){
        
         new SujetForm(disp);
    
    }
    
     if(list.getSelectedIndex()==1){
        
       new DynamicMapForm(disp);
    
    }
    }
    
}
