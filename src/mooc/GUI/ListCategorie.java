/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import mooc.ENTITIES.Categories;
import mooc.HANDLER.CategoriesHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author Anouar
 */
public class ListCategorie extends List implements CommandListener, Runnable {

    Command cmdsup = new Command("Supprimer", Command.SCREEN, 0);
    Command cmdretour = new Command("Retour", Command.SCREEN, 1);
    Command cmdajouter = new Command("Ajouter", Command.SCREEN, 1);
    Categories[] tcategs;
    StringBuffer sb;
    Display disp;

    public ListCategorie(String title, int listType, Display d) {
        super(title, listType);
        disp = d;
        Thread th = new Thread(this);
        th.start();
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdsup) {
            HttpConnection hc;
            DataInputStream dis;
            StringBuffer sb = new StringBuffer();
            int ch;
            try {
                System.out.println("******");
                hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/supprimerCategories.php?catid=" + tcategs[this.getSelectedIndex()].getId());

                dis = new DataInputStream(hc.openDataInputStream());
                while ((ch = dis.read()) != -1) {
                    sb.append((char) ch);
                }
                if ("OK".equals(sb.toString().trim())) {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.CONFIRMATION);
                    a.setTimeout(3000);
                    disp.setCurrent(a);
                } else {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    disp.setCurrent(a);
                }
                sb = new StringBuffer();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            this.deleteAll();
            Thread th = new Thread(this);
            th.start();

        }
        if (cmdretour == c) {
            disp.setCurrent(new Menu2(disp));

        }
        if (cmdajouter == c) {

            disp.setCurrent(new AjouterCategories("Ajouter Categories", disp));

        }

        if (c == List.SELECT_COMMAND) {
//            System.out.println(this.getSelectedIndex());
//System.out.println(people[this.getSelectedIndex()].getTitre());

        }
    }

    public void run() {
        try {
            setCommandListener(this);
            addCommand(cmdsup);
            addCommand(cmdretour);
            addCommand(cmdajouter);

            CategoriesHandler categoriesHandler = new CategoriesHandler();
            // get a parser object
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/listCategories.php");//people.xml est
            DataInputStream dis = new DataInputStream(hc.openDataInputStream());
            parser.parse(dis, categoriesHandler);
            // display the result
            tcategs = categoriesHandler.getCategories();
            // System.out.println("++++++++++++++"+people.length);

            if (tcategs.length > 0) {
                for (int i = 0; i < tcategs.length; i++) {
                    append(tcategs[i].toString(), null);
                }

            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
