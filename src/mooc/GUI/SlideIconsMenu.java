/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

/**
 *
 * @author Firas
 */
import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

public class SlideIconsMenu 
{
	// selected item index
	public int selectedIndex = 0;
	
	// icon label color
	public int textColor = 0xff0000;
	
	// menu bg color
	public int bgColor = 0xffffff;
	
	// icon label font
	public Font textFont = Font.getDefaultFont();
	
	// menu right and left Images
	public Image slideRightImage = null;
	public Image slideLeftImage = null;

	// menu size
	int width = 0;
	int height = 0;
	
	// item labels
	String[] labels = null;
	// item icons
	Image[] icons = null;

	// previous item index (during menu translation)
	int prevIndex = 0;
	
	// menu sliding translation properties
	public int translationDuration = 500;
	long startTranslationTime = 0;
	
	public SlideIconsMenu(String[] labels, Image[] icons, int width, int height) throws Exception
	{
		this.width = width;
		this.height = height;
		
		this.labels = labels;
		this.icons = icons;
		
		slideRightImage = Image.createImage("/mooc/IMAGE/right.png");
		slideLeftImage = Image.createImage("/mooc/IMAGE/left.png");
	}
	public void slideItem(int delta)
	{
		if(!isTranslating() && selectedIndex + delta >= 0 && selectedIndex + delta < labels.length)
		{
			prevIndex = selectedIndex;
			
			selectedIndex += delta;
			
			startTranslationTime = System.currentTimeMillis();
		}
	}
	public boolean isTranslating()
	{
		return prevIndex != selectedIndex;
	}

	public void paint(Graphics g)
	{	
		g.setColor(bgColor);
		g.fillRect(0, 0, width, height);
		
		g.setColor(textColor);
		
		if(selectedIndex > 0)
		{
			g.drawImage(slideLeftImage, 2, height / 2, Graphics.LEFT | Graphics.VCENTER);
		}
		if(selectedIndex < icons.length - 1)
		{
			g.drawImage(slideRightImage, width - 2, height / 2, Graphics.RIGHT | Graphics.VCENTER);
		}
		g.drawString(labels[selectedIndex], width / 2, height - 2, Graphics.BOTTOM | Graphics.HCENTER);
		
		g.setClip(slideLeftImage.getWidth(), 0, width - 2 * slideLeftImage.getWidth(), height);
		
		if(selectedIndex != prevIndex)
		{
			int diff = (int)(System.currentTimeMillis() - startTranslationTime);
			
			if(diff > translationDuration)
			{
				diff = translationDuration;
			}
			
			int coeff = selectedIndex > prevIndex ? 1 : - 1;
			int currentX = width / 2 - coeff * diff * width / translationDuration;
			int nextX = currentX + width * coeff;
			
			g.drawImage(icons[prevIndex], currentX, height / 2, Graphics.VCENTER | Graphics.HCENTER);
			
			g.drawImage(icons[selectedIndex], nextX, height / 2, Graphics.VCENTER | Graphics.HCENTER);
			
			if(diff >= translationDuration)
			{
				prevIndex = selectedIndex;
			}
		}
		else
		{
			g.drawImage(icons[selectedIndex], width / 2, height / 2, Graphics.VCENTER | Graphics.HCENTER);
		}
	}
}
