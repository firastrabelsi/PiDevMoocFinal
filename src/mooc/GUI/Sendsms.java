package mooc.GUI;

import javax.microedition.io.*;
import javax.microedition.lcdui.*;
import javax.wireless.messaging.*;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Date;
import javax.microedition.io.CommConnection;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Choice;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.DateField;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextField;
import mooc.ENTITIES.Reclamation;

public class Sendsms extends Form implements CommandListener, Runnable,MessageListener {

    private TextField toWhom;
    private TextField message;
    private Alert alert;
    private Command send, exit;
    MessageConnection clientConn;

    public Sendsms(String titre) {
        super(titre);
        toWhom = new TextField("To", "", 20, TextField.PHONENUMBER);
        message = new TextField("Message", "", 600, TextField.ANY);
        send = new Command("Send", Command.BACK, 0);
        exit = new Command("Exit", Command.SCREEN, 5);
      //  this.append(toWhom);
        this.append(message);
        this.addCommand(send);
        this.addCommand(exit);
        this.setCommandListener(this);
    }

    public void commandAction(Command cmd, Displayable disp) {
        if (cmd == exit) {
        }
        if (cmd == send) {
            Thread th = new Thread(this);
            th.start();
        }
    }

    public void run() {
        String mno = "123456790";
        String msg = message.getString();
        if (mno.equals("")) {
            alert = new Alert("Alert");
            alert.setString("Enter Mobile Number!!!");
            alert.setTimeout(2000);
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(alert);

        } else {
            try {
                clientConn = (MessageConnection) Connector.open("sms://+" + mno);
            } catch (Exception e) {
                alert = new Alert("Alert");
                alert.setString("Unable to connect to Station because of network problem");
                alert.setTimeout(2000);
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(alert);
            }
            try {
                TextMessage textmessage = (TextMessage) clientConn.newMessage(MessageConnection.TEXT_MESSAGE);
                textmessage.setAddress("sms://" + mno);
                textmessage.setPayloadText(msg);
                clientConn.send(textmessage);
            } catch (Exception e) {
                Alert alert = new Alert("Alert", "", null, AlertType.INFO);
                alert.setTimeout(Alert.FOREVER);
                alert.setString("Unable to send");
                              mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(alert);
            }
                                          mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListReclamationNonValider("Reclamation", List.IMPLICIT));

        }
    }

    public void notifyIncomingMessage(MessageConnection mc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
