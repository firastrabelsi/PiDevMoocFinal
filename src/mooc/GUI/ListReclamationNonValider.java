/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import mooc.ENTITIES.Reclamation;
import mooc.ENTITIES.User;
import mooc.HANDLER.ReclamationHandler;
import mooc.HANDLER.UserHandler;
import org.xml.sax.SAXException;

/**
 *
 * @author nader
 */
public class ListReclamationNonValider extends List implements CommandListener, Runnable {

    Command cmdRefresh = new Command("Actualiser", Command.SCREEN, 0);
    Command cmdconsulter = new Command("Consulter", Command.SCREEN, 0);
    Command cmdmenue = new Command("Retour", Command.SCREEN, 0);
    Reclamation[] reclamation;
    StringBuffer sb;
    Display disp;

    public ListReclamationNonValider(String title, int listType) {
        super(title, listType);
        Thread th = new Thread(this);
        th.start();
    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdRefresh) {
            this.deleteAll();
            Thread th = new Thread(this);
            th.start();
        }
        else if (c==cmdconsulter)
        {
            String indexs=(this.getString(this.getSelectedIndex())).substring(0, this.getString(this.getSelectedIndex()).indexOf(" "));
            int index = Integer.parseInt(indexs)-1;
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ConsulterReclamation("Reclamation",reclamation[index]) );
        }
        else if (c==cmdmenue)
        {
           
            mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new MenuValiderReclamation() );
        }
    }

    public void run() {

        try {
            this.setCommandListener(this);
            this.addCommand(cmdconsulter);
            this.setCommandListener(this);

            this.addCommand(cmdRefresh);
            this.addCommand(cmdmenue);
            ReclamationHandler reclamationhandler = new ReclamationHandler();
            // get a parser object

            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();

            // get an InputStream from somewhere (could be HttpConnection, for example)
            HttpConnection hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/listreclamation.php?");//people.xml est un exemple

            DataInputStream dis = new DataInputStream(hc.openDataInputStream());

            parser.parse(dis, reclamationhandler);

            // display the result
            reclamation = reclamationhandler.getreclamation();

            if (reclamation.length > 0) {
                for (int i = 0; i < reclamation.length; i++) {
                    if(reclamation[i].getEtat().equals("Attente"))
                    {
                    this.append((i+1)+"  :  "+Integer.toString(reclamation[i].getId()), null);
                }
                }

            } else {
               
            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
