/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import mooc.ENTITIES.Reclamation;
import mooc.ENTITIES.User;

/**
 *
 * @author Firas
 */
public class ConsulterOrganisme extends Form implements CommandListener, Runnable {

    HttpConnection hc;
    DataInputStream dis;
    StringBuffer sb;

    Command cmdvalider = new Command("Valider", Command.OK, 0);
    Command cmdsupprimer = new Command("Supprimer", Command.OK, 0);
    Command cmdback = new Command("Retour", Command.BACK, 0);
    int id;
    String etat;

    public ConsulterOrganisme(String title, User r) {
        super(title);
        id = r.getId();
        etat = r.getEtat();
        this.append("Pseudo : " + r.getUsername());
        this.append("Nom du societe : " + r.getEmail());

        this.append("Email : " + r.getEmail());

        this.append("Adresse : " + r.getAdresse());

        this.addCommand(cmdback);
        if (r.getEtat().equals("non valide")) {
            this.addCommand(cmdvalider);
            this.addCommand(cmdsupprimer);
        } else if (r.getEtat().equals("valide")) {
            this.append("Site : " + r.getSite());
            this.append("N telephone : " + r.getNum());
            this.append("Certificat : " + r.getCertificat());
        }
        this.setCommandListener(this);

    }

    public void commandAction(Command c, Displayable d) {
        if (c == cmdvalider) {
            try {
                this.setCommandListener(this);

                hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/validerOrganisme.php?id=" + id);
                dis = hc.openDataInputStream();

                int ascii;
                sb = new StringBuffer();

                while ((ascii = dis.read()) != -1) {

                    sb.append((char) ascii);

                }
                if (sb.toString().equals("Valider")) {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(a);
                } else {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(a);

                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
        } else if (c == cmdback) {
            if (etat.equals("valide")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListeOrganismeValide("Organisme non valide", List.IMPLICIT));
            } else {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListeOrganismeNonValider("Organisme non valide", List.IMPLICIT));

            }
        } else if (c == cmdsupprimer) {
            try {
                this.setCommandListener(this);

                hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/supprimerorganisme.php?id=" + id);
                dis = hc.openDataInputStream();

                int ascii;
                sb = new StringBuffer();

                while ((ascii = dis.read()) != -1) {

                    sb.append((char) ascii);

                }
                if (sb.toString().equals("Suuprimer")) {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(a);
                } else {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(a);

                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListeOrganismeNonValider("Organisme non valide", List.IMPLICIT));
        }
    }

    public void run() {

    }

}
