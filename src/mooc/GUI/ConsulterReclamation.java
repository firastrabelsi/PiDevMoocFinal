/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mooc.GUI;

import java.io.DataInputStream;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.Alert;
import javax.microedition.lcdui.AlertType;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import mooc.ENTITIES.Reclamation;

/**
 *
 * @author Firas
 */
public class ConsulterReclamation extends Form implements CommandListener, Runnable {
    
    HttpConnection hc;
    DataInputStream dis;
    StringBuffer sb;
    Command cmdvalider = new Command("Valider", Command.OK, 0);
    Command cmdback = new Command("Retour", Command.BACK, 0);
    int id;
    String rolee;

    public ConsulterReclamation(String title, Reclamation r) {
        super(title);
        id = r.getId();
        rolee = r.getEtat();
        this.append("Categorie : " + r.getCategorie());
        this.append("\n");
        this.append("Email : " + r.getEmail());
        this.append("\n");
        
        this.append("N telephone : " + r.getTel());
        this.append("\n");
        
        this.append("Message : " + r.getText());
        this.append("\n");
        this.append("\n");
        
        this.append("Date : " + r.getDate());
        this.addCommand(cmdback);
       if(r.getEtat().equals("Attente")) {
            this.addCommand(cmdvalider);
        }
        
        this.setCommandListener(this);
        
    }
    
    public void commandAction(Command c, Displayable d) {
        if (c == cmdvalider) {
            try {
                this.setCommandListener(this);
                
                hc = (HttpConnection) Connector.open("http://localhost/MoocPHPJ2ME/validerreclamation.php?id=" + id);
                dis = hc.openDataInputStream();
                
                int ascii;
                sb = new StringBuffer();
                
                while ((ascii = dis.read()) != -1) {
                    
                    sb.append((char) ascii);
                    
                }
                if (sb.toString().equals("Reclamation Valider")) {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(a);
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new Sendsms("Informer"));
                    
                } else {
                    Alert a = new Alert("Information", sb.toString(), null, AlertType.ERROR);
                    a.setTimeout(3000);
                    mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(a);
                    
                }
                
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            ;
        } else if (c == cmdback) {
            if (rolee.equals("Attente")) {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListReclamationNonValider("Reclamation", List.IMPLICIT));
            }
            else
            {
                mooc.MIDLET.LoginMIDLET.mMidlet.display.setCurrent(new ListReclamation("Reclamation", List.IMPLICIT));

            }
        }
    }
    
    public void run() {
        
    }
    
}
